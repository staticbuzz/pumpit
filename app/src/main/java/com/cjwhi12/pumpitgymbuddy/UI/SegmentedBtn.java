package com.cjwhi12.pumpitgymbuddy.UI;

/**
 * Created by Christopher on 10/05/2015.
 * With much inspiration from http://blog.bookworm.at/2010/10/segmented-controls-in-android.html#!
 */
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.widget.RadioButton;

import com.cjwhi12.pumpitgymbuddy.R;

public class SegmentedBtn extends RadioButton {

    private float mX;

    public SegmentedBtn(Context context) {
        super(context);
    }

    public SegmentedBtn(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SegmentedBtn(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    public void onDraw(Canvas canvas) {

        String text = this.getText().toString();
        Paint textPaint = new Paint();
        textPaint.setAntiAlias(true);
        float currentWidth = textPaint.measureText(text);
        float currentHeight = textPaint.measureText("x");

        textPaint.setTextSize(this.getTextSize());
        textPaint.setTextAlign(Paint.Align.CENTER);


        textPaint.setColor(Color.WHITE);

        if (this.isChecked()) {

            this.setBackgroundColor(getResources().getColor(R.color.accent));

        } else {
            this.setBackgroundColor(getResources().getColor(R.color.dk_gray));
            textPaint.setColor(getResources().getColor(R.color.lt_gray));
        }

        float w = (this.getWidth() / 2) - currentWidth;
        float h = (this.getHeight() / 2) + currentHeight +8;
        canvas.drawText(text, mX, h, textPaint);

    }

    @Override
    protected void onSizeChanged(int w, int h, int ow, int oh) {
        super.onSizeChanged(w, h, ow, oh);
        mX = w * 0.5f; // remember the center of the screen
    }

}
