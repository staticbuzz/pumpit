package com.cjwhi12.pumpitgymbuddy;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cjwhi12.pumpitgymbuddy.Activities.ChooseSplitForWorkoutActivity;
import com.cjwhi12.pumpitgymbuddy.Activities.ManageSplitsActivity;
import com.cjwhi12.pumpitgymbuddy.Activities.ProgressOptionsActivity;
import com.cjwhi12.pumpitgymbuddy.Activities.ScheduleActivity;
import com.cjwhi12.pumpitgymbuddy.Fragments.NewProfileFragment;
import com.cjwhi12.pumpitgymbuddy.Fragments.WelcomeFragment;
import com.cjwhi12.pumpitgymbuddy.ListAdapters.ActionListAdapter;
import com.cjwhi12.pumpitgymbuddy.databasehelpers.DatabaseHelper;
import com.cjwhi12.pumpitgymbuddy.models.Action;
import com.cjwhi12.pumpitgymbuddy.models.Exercise;
import com.cjwhi12.pumpitgymbuddy.models.Schedule;
import com.cjwhi12.pumpitgymbuddy.models.User;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;


public class MainActivity extends Activity {
    private Fragment fragment;
    private User user;
    private DatabaseHelper dbh;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private ListAdapter adapter;
    private Action[] actions = new Action[5];
    private SharedPreferences settings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionsHolder actionsHolder = new ActionsHolder(this);
        actions = actionsHolder.getActions();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_actions);

        // setting the nav drawer list adapter
        adapter = new ActionListAdapter(getApplicationContext(),
                actions);
        mDrawerList.setAdapter(adapter);



        // enabling action bar app icon and behaving it as toggle button
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ){
            public void onDrawerClosed(View view) {
                getActionBar().setTitle("Home");
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle("Actions");
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        settings = getSharedPreferences("PumpItGymBuddy.AppData", 0);
        SharedPreferences.Editor editor = settings.edit();

        // Add “profileExists” boolean to preferences
        //editor.putBoolean("profileExists", false);
        //editor.commit();

        dbh = new DatabaseHelper(this);

        //DEBUGGING
        //dbh.deleteWorkouts();
        //dbh.removeUser();

        boolean profileExists = settings.getBoolean("profileExists", false);


        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();


        int numExercises = dbh.getExercisesCount();
        if (numExercises == 0)
        {
            saveDefaultExercisesToDatabase();
        }

        int numSchedules = dbh.getScheduleCount();
        if (numSchedules == 0)
        {
            saveDefaultSchedulesToDatabase();
        }

        if (!profileExists) {

            fragment = new NewProfileFragment();

            getActionBar().setTitle("Setup Profile");
            fragmentTransaction.add(R.id.frame_container, fragment);
            fragmentTransaction.commit();
        }

        else
        {
            fragment = new WelcomeFragment();
            user = dbh.getUser();
            Bundle bundle = new Bundle();
            bundle.putString("name", user.getName());
            fragment.setArguments(bundle);
            getActionBar().setTitle("Home");
            fragmentTransaction.add(R.id.frame_container, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
    }
    @Override
    public void onRestoreInstanceState(Bundle inState){
        super.onRestoreInstanceState(inState);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_workout:
                Intent i = new Intent(this,ChooseSplitForWorkoutActivity.class);
                startActivity(i);
                return true;
            case R.id.action_settings:
                displayAboutDialog();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /***
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //boolean to determine if drawer is open or closed.
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        //hide the Workout menu button if the drawer *IS* open
        menu.findItem(R.id.action_workout).setVisible(!drawerOpen);
        //hide the Settings menu button if the drawer *ISN'T* open
        menu.findItem(R.id.action_settings).setVisible(drawerOpen);
        if (!settings.getBoolean("profileExists", false)){
            menu.findItem(R.id.action_workout).setVisible(false);
            getActionBar().hide();
        }
        if (dbh.getSplitsCount() == 0)
            getActionBar().hide();

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setTitle(CharSequence title) {
        getActionBar().setTitle(title);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggle
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private class DrawerItemClickListener implements AbsListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    public void selectItem(int id){

        Intent i;
        switch (id) {
            case 0:
                Log.i("Fragment ID",id + "");
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragment = new WelcomeFragment();
                Bundle bundle = new Bundle();
                bundle.putString("name", user.getName());
                fragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.frame_container, fragment);
                mDrawerLayout.closeDrawer(mDrawerList);

                fragmentTransaction.commit();
                break;

            case 1:
                i = new Intent(this, ChooseSplitForWorkoutActivity.class);
                startActivity(i);

                break;

            case 2:
                i = new Intent(this, ManageSplitsActivity.class);
                startActivity(i);

                break;

            case 3:
                i = new Intent(this, ProgressOptionsActivity.class);
                startActivity(i);

                break;

            case 4:
                i = new Intent(this, ScheduleActivity.class);
                startActivity(i);

                break;
            }
    }

    //TODO: make this an asynchtask
    public String loadDefaultExercises()
    {
        String json = null;
        try {

            InputStream is = getAssets().open("exercises.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            Toast.makeText(this,"Error reading default exercises from file!",Toast.LENGTH_LONG).show();
            return null;
        }
        return json;
    }

    public void saveDefaultSchedulesToDatabase(){
        String[] days = new String[]{"Mon","Tue","Wed","Thu","Fri","Sat","Sun"};
        for (String day:days)
        {
            dbh.addSchedule(new Schedule(day));
        }
    }

    //TODO: make this an asynchtask
    public void saveDefaultExercisesToDatabase() {
        final int LEGS = 0;
        final int ARMS = 1;
        final int CHEST = 2;
        final int BACK = 3;
        final int SHOULDERS = 4;
        final int ABS = 5;
        final int CARDIO = 6;

        String JSON = loadDefaultExercises();
        try {
            JSONObject JO = new JSONObject(JSON);
            JSONArray categories = JO.getJSONArray("categories");
            JSONObject catObj;
            JSONArray exercises;
            JSONObject exObj;
            String name, unit, category;
            category = "";
            for (int i = 0; i < categories.length(); i++)
            {
                if (i == LEGS)
                    category = "legs";
                else if (i == ARMS)
                    category = "arms";
                else if (i == CHEST)
                    category = "chest";
                else if (i == BACK)
                    category = "back";
                else if (i == SHOULDERS)
                    category = "shoulders";
                else if (i == ABS)
                    category = "abs";
                else if (i == CARDIO)
                    category = "cardio";

                catObj = categories.getJSONObject(i);
                exercises = catObj.getJSONArray("exercises");
                for (int j = 0; j < exercises.length(); j++) {
                    exObj = exercises.getJSONObject(j);
                    name = exObj.getString("name");
                    unit = exObj.getString("unit");
                    dbh.addExercise(new Exercise(name, unit, category));
                }
            }
        }
        catch(JSONException e)
        {
            e.printStackTrace();
            Toast.makeText(this,"Error parsing JSON",Toast.LENGTH_LONG).show();
        }
    }

    private void displayAboutDialog() {
        //inflate the view
        View view = getLayoutInflater().inflate(R.layout.about, null, false);
        TextView textView = (TextView) view.findViewById(R.id.about_credits);
        int defaultColor = textView.getTextColors().getDefaultColor();
        textView.setTextColor(defaultColor);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view).create().show();
    }

}
