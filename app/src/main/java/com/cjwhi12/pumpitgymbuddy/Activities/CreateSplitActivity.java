package com.cjwhi12.pumpitgymbuddy.Activities;

import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.cjwhi12.pumpitgymbuddy.R;
import com.cjwhi12.pumpitgymbuddy.databasehelpers.DatabaseHelper;
import com.cjwhi12.pumpitgymbuddy.models.Split;

public class CreateSplitActivity extends Activity {

    private ImageButton iconBtn,iconBtn2,iconBtn3,iconBtn4,iconBtn5,iconBtn6;
    private ImageButton iconBtn7,iconBtn8,iconBtn9,iconBtn10,iconBtn11,iconBtn12;
    private EditText nameTxt;
    private DatabaseHelper dbh;

    //split variables
    String name, icon;
    Split createdSplit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_split);
        dbh = new DatabaseHelper(this);

        nameTxt = (EditText)findViewById(R.id.nameTxt);
        iconBtn = (ImageButton)findViewById(R.id.imageButton);
        iconBtn.setOnClickListener(iconClicked);

        iconBtn2 = (ImageButton)findViewById(R.id.imageButton2);
        iconBtn2.setOnClickListener(iconClicked);

        iconBtn3 = (ImageButton)findViewById(R.id.imageButton3);
        iconBtn3.setOnClickListener(iconClicked);

        iconBtn4 = (ImageButton)findViewById(R.id.imageButton4);
        iconBtn4.setOnClickListener(iconClicked);

        iconBtn5 = (ImageButton)findViewById(R.id.imageButton5);
        iconBtn5.setOnClickListener(iconClicked);

        iconBtn6 = (ImageButton)findViewById(R.id.imageButton6);
        iconBtn6.setOnClickListener(iconClicked);

        iconBtn7 = (ImageButton)findViewById(R.id.imageButton7);
        iconBtn7.setOnClickListener(iconClicked);

        iconBtn8 = (ImageButton)findViewById(R.id.imageButton8);
        iconBtn8.setOnClickListener(iconClicked);

        iconBtn9 = (ImageButton)findViewById(R.id.imageButton9);
        iconBtn9.setOnClickListener(iconClicked);

        iconBtn10 = (ImageButton)findViewById(R.id.imageButton10);
        iconBtn10.setOnClickListener(iconClicked);

        iconBtn11 = (ImageButton)findViewById(R.id.imageButton11);
        iconBtn11.setOnClickListener(iconClicked);

        iconBtn12 = (ImageButton)findViewById(R.id.imageButton12);
        iconBtn12.setOnClickListener(iconClicked);

        iconBtn.setBackground(getResources().getDrawable(R.drawable.border_white));
        iconBtn2.setBackground(getResources().getDrawable(R.drawable.border_black));
        iconBtn3.setBackground(getResources().getDrawable(R.drawable.border_black));
        iconBtn4.setBackground(getResources().getDrawable(R.drawable.border_black));
        iconBtn5.setBackground(getResources().getDrawable(R.drawable.border_black));
        iconBtn6.setBackground(getResources().getDrawable(R.drawable.border_black));
        iconBtn7.setBackground(getResources().getDrawable(R.drawable.border_black));
        iconBtn8.setBackground(getResources().getDrawable(R.drawable.border_black));
        iconBtn9.setBackground(getResources().getDrawable(R.drawable.border_black));
        iconBtn10.setBackground(getResources().getDrawable(R.drawable.border_black));
        iconBtn11.setBackground(getResources().getDrawable(R.drawable.border_black));
        iconBtn12.setBackground(getResources().getDrawable(R.drawable.border_black));

        //set icon to first button as default
        icon = "biceps";
    }

    private View.OnClickListener iconClicked = new View.OnClickListener() {
        public void onClick(View view) {
            iconBtn.setBackground(getResources().getDrawable(R.drawable.border_black));
            iconBtn2.setBackground(getResources().getDrawable(R.drawable.border_black));
            iconBtn3.setBackground(getResources().getDrawable(R.drawable.border_black));
            iconBtn4.setBackground(getResources().getDrawable(R.drawable.border_black));
            iconBtn5.setBackground(getResources().getDrawable(R.drawable.border_black));
            iconBtn6.setBackground(getResources().getDrawable(R.drawable.border_black));
            iconBtn7.setBackground(getResources().getDrawable(R.drawable.border_black));
            iconBtn8.setBackground(getResources().getDrawable(R.drawable.border_black));
            iconBtn9.setBackground(getResources().getDrawable(R.drawable.border_black));
            iconBtn10.setBackground(getResources().getDrawable(R.drawable.border_black));
            iconBtn11.setBackground(getResources().getDrawable(R.drawable.border_black));
            iconBtn12.setBackground(getResources().getDrawable(R.drawable.border_black));

            switch (view.getId()) {
                case R.id.imageButton:
                    iconBtn.setBackground(getResources().getDrawable(R.drawable.border_white));
                    icon = "biceps";
                    break;
                case R.id.imageButton2:
                    iconBtn2.setBackground(getResources().getDrawable(R.drawable.border_white));
                    icon = "leg";
                    break;
                case R.id.imageButton3:
                    iconBtn3.setBackground(getResources().getDrawable(R.drawable.border_white));
                    icon = "chest";
                    break;
                case R.id.imageButton4:
                    iconBtn4.setBackground(getResources().getDrawable(R.drawable.border_white));
                    icon = "back";
                    break;
                case R.id.imageButton5:
                    iconBtn5.setBackground(getResources().getDrawable(R.drawable.border_white));
                    icon = "shoulders";
                    break;
                case R.id.imageButton6:
                    iconBtn6.setBackground(getResources().getDrawable(R.drawable.border_white));
                    icon = "abs";
                    break;
                case R.id.imageButton7:
                    iconBtn7.setBackground(getResources().getDrawable(R.drawable.border_white));
                    icon = "cardio";
                    break;
                case R.id.imageButton8:
                    iconBtn8.setBackground(getResources().getDrawable(R.drawable.border_white));
                    icon = "weightlifter3";
                    break;
                case R.id.imageButton9:
                    iconBtn9.setBackground(getResources().getDrawable(R.drawable.border_white));
                    icon = "favorite21";
                    break;
                case R.id.imageButton10:
                    iconBtn10.setBackground(getResources().getDrawable(R.drawable.border_white));
                    icon = "dumbbell";
                    break;
                case R.id.imageButton11:
                    iconBtn11.setBackground(getResources().getDrawable(R.drawable.border_white));
                    icon = "scale17";
                    break;
                case R.id.imageButton12:
                    iconBtn12.setBackground(getResources().getDrawable(R.drawable.border_white));
                    icon = "user157";
                    break;
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_split, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_done:
                name = nameTxt.getText().toString();
                if (!name.equals("")) {
                    createdSplit = new Split(name, dbh.getUser().getName(), icon);
                    dbh = new DatabaseHelper(this);
                    if (dbh.addSplit(createdSplit)) {
                        createdSplit = dbh.getNewlyCreatedSplit();
                        Intent i = new Intent(this, ExerciseCategoryActivity.class);
                        i.putExtra("createdSplit", createdSplit);
                        startActivity(i);
                    }
                    else
                        Toast.makeText(this, "Error creating split... Try again", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(this, "A split's gotta have a name, buddy", Toast.LENGTH_SHORT).show();
                    nameTxt.requestFocus();
                }
        }
        return super.onOptionsItemSelected(item);
    }
}
