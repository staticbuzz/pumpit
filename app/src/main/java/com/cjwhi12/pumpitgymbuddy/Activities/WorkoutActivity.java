package com.cjwhi12.pumpitgymbuddy.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.cjwhi12.pumpitgymbuddy.ListAdapters.WorkoutAdapter;
import com.cjwhi12.pumpitgymbuddy.MainActivity;
import com.cjwhi12.pumpitgymbuddy.R;
import com.cjwhi12.pumpitgymbuddy.databasehelpers.DatabaseHelper;
import com.cjwhi12.pumpitgymbuddy.models.Exercise;
import com.cjwhi12.pumpitgymbuddy.models.Split;
import com.cjwhi12.pumpitgymbuddy.models.WorkoutResult;

import java.util.ArrayList;
import java.util.Calendar;

public class WorkoutActivity extends Activity {

    private ArrayList<WorkoutResult> results;
    private ListAdapter mAdapterExercises;
    private DatabaseHelper dbh;
    private String username;

    // params
    private Split selectedSplit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout);

        dbh = new DatabaseHelper(this);
        dbh.deleteNullWorkoutResults();

        selectedSplit = getIntent().getParcelableExtra("selectedSplit");
        long split_id = selectedSplit.getId();
        ArrayList<Exercise> exercises = new ArrayList<>(dbh.getAllExercisesForSplit(split_id).values());
        results = new ArrayList<>();
        ArrayList<WorkoutResult> prevResults = new ArrayList<>();
        for (Exercise e: exercises){
            //populate results array with a WorkoutResult (0's for stats) that can be updated later
            results.add(new WorkoutResult(e.getId(), split_id,0,0,username));
            //populate prevResults array with previous WorkoutResults for each exercise
            prevResults.add(dbh.getLastWorkoutResultForExercise(e.getId()));
        }

        TextView splitName = (TextView) findViewById(R.id.splitNameTxt);
        splitName.setText(selectedSplit.getName());

        ImageView icon = (ImageView) findViewById(R.id.splitIconIV);

        Resources res = getResources();
        String drawableName = selectedSplit.getIcon();
        int resID = res.getIdentifier(drawableName, "drawable", getPackageName());
        Drawable drawable = res.getDrawable(resID);
        icon.setImageDrawable(drawable);

        AbsListView mListViewExecises = (AbsListView) findViewById(R.id.exercisesLV);

        mAdapterExercises = new WorkoutAdapter(this, exercises,results, prevResults);

        mListViewExecises.setAdapter(mAdapterExercises);

        //add a click listener to list view items that will show dialog fragment for inputting results
        mListViewExecises.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                Exercise e = (Exercise) mAdapterExercises.getItem(position);
                // get and inflate the layout xml for the dialog view
                LayoutInflater layoutInflater = LayoutInflater.from(WorkoutActivity.this);
                View dialogView = layoutInflater.inflate(R.layout.dialog_input_workout_results, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(WorkoutActivity.this);
                //apply the layout xml to the dialog
                alertDialogBuilder.setView(dialogView);

                final EditText unitInputTxt = (EditText) dialogView.findViewById(R.id.unitInputTxt);
                final EditText repsInputTxt = (EditText) dialogView.findViewById(R.id.repsInputTxt);

                // setup a dialog window for inputting results and reps
                alertDialogBuilder.setCancelable(false)
                        //set to null as we override this later
                        .setPositiveButton("OK", null)
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });


                // create the actual alert dialog then show it
                final AlertDialog alert = alertDialogBuilder.create();
                alert.show();
                unitInputTxt.requestFocus();
                //override the positive button so it doesn't immediately close before validation
                alert.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //use this to determine if we should dismiss the dialog or keep it open
                        boolean closeMe = false;

                        //make sure input is numeric or decimal
                        try {
                            results.get(position).setResult(Double.parseDouble(unitInputTxt.getText().toString()));
                            results.get(position).setReps(Integer.parseInt(repsInputTxt.getText().toString()));
                            closeMe = true;
                        } catch (NumberFormatException e) {
                            Toast.makeText(WorkoutActivity.this, "Numbers only, buddy", Toast.LENGTH_SHORT).show();
                        }
                        //only dismiss on OK button press if both EditTexts contain numbers
                        if (closeMe)
                            alert.dismiss();
                    }
                });
            }


        });
    }

    public void saveWorkoutResults(){
        for (WorkoutResult r: results){
            dbh.addWorkoutResult(new WorkoutResult(r.getExerciseId(),r.getSplitId(),r.getResult(),r.getReps(),username));
        }
        Calendar c = Calendar.getInstance();
        selectedSplit.setDateLast(c.getTime());
        if (dbh.updateSplit(selectedSplit))
            Toast.makeText(this, "Workout results saved", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "Error saving workout results", Toast.LENGTH_SHORT).show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_workout, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_save) {
            saveWorkoutResults();
            Intent i = new Intent(WorkoutActivity.this, MainActivity.class);
            startActivity(i);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
