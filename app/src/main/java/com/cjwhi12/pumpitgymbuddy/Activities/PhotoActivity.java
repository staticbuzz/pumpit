package com.cjwhi12.pumpitgymbuddy.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.cjwhi12.pumpitgymbuddy.R;
import com.cjwhi12.pumpitgymbuddy.databasehelpers.DatabaseHelper;
import com.cjwhi12.pumpitgymbuddy.models.Photo;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class PhotoActivity extends Activity {

    static final int TAKE_PHOTO_CODE = 1;
    static final int RESULT_LOAD_IMAGE = 2;

    static final String appPhotoDir = "/Pictures/PumpItGymBuddy/";

    private ImageView imageView;
    private String dir;
    private String currentPhotoPath;
    private DatabaseHelper dbh;
    private RadioButton beforeRad;
    private RadioButton afterRad;
    private int photosCount;
    private TextView dateTakenTxt;
    private Photo p;
    private DateFormat df;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);

        dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/PumpItGymBuddy/";
        File newdir = new File(dir);
        newdir.mkdirs();

        beforeRad = (RadioButton) findViewById(R.id.radioButton);
        afterRad = (RadioButton) findViewById(R.id.radioButton2);
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.photoGroup);
        dateTakenTxt = (TextView) findViewById(R.id.dateTakenTxt);
        imageView = (ImageView) findViewById(R.id.thumbnailImageView);
        dbh = new DatabaseHelper(this);
        df = DateFormat.getDateInstance();
        dateTakenTxt.setText("");
        checkPhotoCount();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioButton) {
                    p = dbh.getInitialPhoto();
                    if (p != null && !p.getFilePath().isEmpty()) {
                        //load the image, convert to scaled bitmap and display it
                        File file = new File(Environment.getExternalStorageDirectory() + appPhotoDir + p.getFilePath());
                        imageView.setImageBitmap(decodeSampledBitmapFromFile(file.getAbsolutePath(), 800, 600));
                    }
                } else {
                    p = dbh.getLatestPhoto();
                    if (p != null && !p.getFilePath().isEmpty()) {
                        //load the image, convert to scaled bitmap and display it
                        File file = new File(Environment.getExternalStorageDirectory() + appPhotoDir + p.getFilePath());
                        imageView.setImageBitmap(decodeSampledBitmapFromFile(file.getAbsolutePath(), 800, 600));
                    }
                    imageView.invalidate();
                }
                dateTakenTxt.setText(df.format(p.getDateTaken()));

            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (photosCount > 0) {
                    File file = new File(Environment.getExternalStorageDirectory() + appPhotoDir + p.getFilePath());
                    Intent intent = new Intent();
                    intent.setAction(android.content.Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(file), "image/*");
                    startActivity(intent);
                }

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_take_photo) {
            if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {

                //set a timestamp as filename so each one is unique
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                currentPhotoPath = timeStamp + ".jpg";
                String file = dir+currentPhotoPath;

                File newfile = new File(file);
                try {
                    newfile.createNewFile();
                } catch (IOException e) {
                    Toast.makeText(this,
                            "Something went tits-up. Try again",
                            Toast.LENGTH_SHORT).show();
                }

                Uri outputFileUri = Uri.fromFile(newfile);

                //Load the camera intent using default Android camera app
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

                startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
            }

            else
            {
                Toast.makeText(this,
                        "Your device doesn't seem to have a camera. No happy-snaps for you...",
                        Toast.LENGTH_SHORT).show();
            }
            return true;
        }
        if (id == R.id.action_gallery) {
            Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, RESULT_LOAD_IMAGE);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TAKE_PHOTO_CODE && resultCode == Activity.RESULT_OK) {
            Toast.makeText(this,
                    "Photo saved",
                    Toast.LENGTH_SHORT).show();

            //load the image, convert to scaled bitmap and display it
            File file = new File(Environment.getExternalStorageDirectory()+ appPhotoDir + currentPhotoPath);
            imageView.setImageBitmap(decodeSampledBitmapFromFile(file.getAbsolutePath(), 800, 400));

            //add the photo to the database
            Calendar cal = Calendar.getInstance();
            Date now = cal.getTime();
            Photo p = new Photo(currentPhotoPath,now,dbh.getUser().getName());
            dbh.addPhoto(p);
            checkPhotoCount();
        }

        //if everything is working and the user clicked an image
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && data != null)
        {
            // Get the Image from data
            Uri clickedImg = data.getData();
            String[] colFilepath = { MediaStore.Images.Media.DATA };

            // Get the cursor
            Cursor cursor = getContentResolver().query(clickedImg,
                    colFilepath, null, null, null);
            // Move to first row
            cursor.moveToFirst();
            //Find the index which has the filepath and save the value to string
            String imgFilepath = cursor.getString(cursor.getColumnIndex(colFilepath[0]));
            //Close the cursor
            cursor.close();

            //create a file from the filepath to send to the intent to view photo
            File imgFile = new File(imgFilepath);

            Intent i = new Intent();
            i.setDataAndType(Uri.fromFile(imgFile),"image/*");
            i.setAction(Intent.ACTION_VIEW);
            startActivity(i);
        }
    }

    public static Bitmap decodeSampledBitmapFromFile(String path,
                                                     int reqWidth, int reqHeight) { // BEST QUALITY MATCH

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // Calculate inSampleSize
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;

        if (height > reqHeight) {
            inSampleSize = Math.round((float)height / (float)reqHeight);
        }

        int expectedWidth = width / inSampleSize;

        if (expectedWidth > reqWidth) {
            inSampleSize = Math.round((float)width / (float)reqWidth);
        }


        options.inSampleSize = inSampleSize;

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeFile(path, options);
    }

    private void checkPhotoCount()
    {
        photosCount = dbh.getPhotosCount();
        if (photosCount == 0)
        {
            beforeRad.setChecked(true);
            afterRad.setEnabled(true);
            afterRad.setChecked(false);
            afterRad.setEnabled(false);

            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("Are you camera shy or something?");
            alertDialog.setMessage("You haven't taken any before/after photos yet. Make yourself pretty and get snapping to help keep you motivated!");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Got It",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
        else if (photosCount == 1)
        {
            beforeRad.setChecked(true);
            beforeRad.setEnabled(true);
            afterRad.setChecked(false);
            afterRad.setEnabled(false);
            p = dbh.getLatestPhoto();
            if (p != null && !p.getFilePath().isEmpty()) {
                //load the image, convert to scaled bitmap and display it
                File file = new File(Environment.getExternalStorageDirectory() + appPhotoDir + p.getFilePath());

                imageView.setImageBitmap(decodeSampledBitmapFromFile(file.getAbsolutePath(), 800, 600));
                dateTakenTxt.setText(df.format(p.getDateTaken()));
            }
        }
        else {
            beforeRad.setChecked(false);
            beforeRad.setEnabled(true);
            afterRad.setChecked(true);
            afterRad.setEnabled(true);
            p = dbh.getLatestPhoto();
            if (p != null && !p.getFilePath().isEmpty()) {
                //load the image, convert to scaled bitmap and display it
                File file = new File(Environment.getExternalStorageDirectory() + appPhotoDir + p.getFilePath());

                imageView.setImageBitmap(decodeSampledBitmapFromFile(file.getAbsolutePath(), 800, 600));
                dateTakenTxt.setText(df.format(p.getDateTaken()));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_photo, menu);
        return true;
    }


}
