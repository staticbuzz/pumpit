package com.cjwhi12.pumpitgymbuddy.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.cjwhi12.pumpitgymbuddy.Fragments.WelcomeFragment;
import com.cjwhi12.pumpitgymbuddy.ListAdapters.SelectSplitAdapter;
import com.cjwhi12.pumpitgymbuddy.R;
import com.cjwhi12.pumpitgymbuddy.databasehelpers.DatabaseHelper;
import com.cjwhi12.pumpitgymbuddy.models.Schedule;
import com.cjwhi12.pumpitgymbuddy.models.Split;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static java.util.Calendar.DAY_OF_WEEK;

public class ChooseSplitForWorkoutActivity extends Activity
{
    private ArrayList<Split> splits;
    private ListAdapter mAdapter;
    private AbsListView mListView;
    private DatabaseHelper dbh;
    private Split nextSchedSplit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_split_for_workout);
        LinearLayout noSplitsLayout = (LinearLayout) findViewById(R.id.noSplitsLayout);
        LinearLayout schedSplitLL = (LinearLayout) findViewById(R.id.schedSplitLL);
        LinearLayout noSchedSplitLL = (LinearLayout) findViewById(R.id.noScheduledSplitLayout);
        nextSchedSplit = null;

        mListView = (AbsListView) findViewById(R.id.editSplitListView);

        dbh = new DatabaseHelper(this);
        HashMap<Long,Split> splitHashMap = dbh.getAllSplits();

        int count = dbh.getSplitsCount();
        if (count == 0)
        {
            mListView.setVisibility(View.GONE);
            noSplitsLayout.setVisibility(View.VISIBLE);
        }
        else
        {
            mListView.setVisibility(View.VISIBLE);
            noSplitsLayout.setVisibility(View.GONE);
        }

        ArrayList<Schedule> schedules = new ArrayList<>(dbh.getAllSchedules().values());
        //SUNDAY = 0
        int numDaysTillSplit = -1;
        List<WelcomeFragment.ScheduleDay> scheduleDays = new ArrayList<>();
        for (int i = schedules.size()-1; i >= 0; i--)
        {
            if (schedules.get(i).getSplitId() != 0) {
                int schedDay = Schedule.checkDay(schedules.get(i).getDay());

                int tempDay = DAY_OF_WEEK + schedDay;
                if (tempDay > 6)
                    tempDay -= 7;
                numDaysTillSplit = tempDay + 1;
                if (numDaysTillSplit > 6)
                    numDaysTillSplit -= 7;

                scheduleDays.add(new WelcomeFragment.ScheduleDay(schedules.get(i).getSplitId(), schedules.get(i).getDay(), numDaysTillSplit));
            }
        }
        Collections.sort(scheduleDays);

        if (scheduleDays.size() > 0) {
            nextSchedSplit = (dbh.getSplit(scheduleDays.get(0).getSplitId()));
        }


        if (nextSchedSplit == null){
            schedSplitLL.setVisibility(View.GONE);
            noSchedSplitLL.setVisibility(View.VISIBLE);

        }
        else {
            splitHashMap.remove(nextSchedSplit.getId());
            schedSplitLL.setVisibility(View.VISIBLE);
            noSchedSplitLL.setVisibility(View.GONE);
            TextView schedName = (TextView)findViewById(R.id.schedSplitName);
            TextView schedCountVal = (TextView)findViewById(R.id.schedSplitCountVal);
            ImageView schedImg = (ImageView)findViewById(R.id.schedSplitImg);
            schedName.setText(nextSchedSplit.getName());
            Resources res = getResources();
            String drawableName = nextSchedSplit.getIcon();
            int resID = res.getIdentifier(drawableName, "drawable", getPackageName());
            Drawable drawable = res.getDrawable(resID);
            schedImg.setImageDrawable(drawable);
            int countEx = dbh.getExerciseCountForSplit(nextSchedSplit.getId());
            schedCountVal.setText(countEx + "");
        }

        splits = new ArrayList<>(splitHashMap.values());

        mAdapter = new SelectSplitAdapter(this, splits);

        // Set the adapter
        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

        mListView.setOnItemClickListener(splitClicked);

        schedSplitLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Split selectedSplit = (Split) nextSchedSplit;
                Intent i = new Intent(ChooseSplitForWorkoutActivity.this, WorkoutActivity.class);
                i.putExtra("selectedSplit", selectedSplit);
                startActivity(i);
            }
        });
    }

    private AdapterView.OnItemClickListener splitClicked = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Split selectedSplit = (Split) mAdapter.getItem(position);
            Intent i = new Intent(ChooseSplitForWorkoutActivity.this, WorkoutActivity.class);
            i.putExtra("selectedSplit", selectedSplit);
            startActivity(i);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_choose_split_for_workout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        return super.onOptionsItemSelected(item);
    }


}
