package com.cjwhi12.pumpitgymbuddy.Activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.cjwhi12.pumpitgymbuddy.AlarmReceiver;
import com.cjwhi12.pumpitgymbuddy.BootReceiver;
import com.cjwhi12.pumpitgymbuddy.ListAdapters.ScheduleSplitAdapter;
import com.cjwhi12.pumpitgymbuddy.MainActivity;
import com.cjwhi12.pumpitgymbuddy.R;
import com.cjwhi12.pumpitgymbuddy.databasehelpers.DatabaseHelper;
import com.cjwhi12.pumpitgymbuddy.models.Schedule;
import com.cjwhi12.pumpitgymbuddy.models.Split;

import java.util.ArrayList;
import java.util.Calendar;

public class ScheduleActivity extends Activity {

    private Split[] scheduledSplits;
    private ArrayList<Schedule> allSchedules;
    private AbsListView mListview;
    private DatabaseHelper dbh;
    private Split selectedSplit;
    private static final int MON=0;
    private static final int TUE=1;
    private static final int WED=2;
    private static final int THU=3;
    private static final int FRI=4;
    private static final int SAT=5;
    private static final int SUN=6;
    private String day;
    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;
    private SharedPreferences settings;
    private RadioButton manualBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        final RadioButton monBtn = (RadioButton) findViewById(R.id.radioButtonMon);
        final RadioButton tueBtn = (RadioButton) findViewById(R.id.radioButtonTue);
        final RadioButton wedBtn = (RadioButton) findViewById(R.id.radioButtonWed);
        final RadioButton thuBtn = (RadioButton) findViewById(R.id.radioButtonThu);
        final RadioButton friBtn = (RadioButton) findViewById(R.id.radioButtonFri);
        final RadioButton satBtn = (RadioButton) findViewById(R.id.radioButtonSat);
        final RadioButton sunBtn = (RadioButton) findViewById(R.id.radioButtonSun);

        manualBtn = (RadioButton) findViewById(R.id.radioButtonManual);
        final RadioButton weeklyBtn = (RadioButton) findViewById(R.id.radioButtonWeekly);
        final RadioGroup schedFreq = (RadioGroup) findViewById(R.id.reportGroup);
        final RadioGroup dayGroup = (RadioGroup) findViewById(R.id.dayGroup);
        final LinearLayout weeklyLL = (LinearLayout) findViewById(R.id.weeklyLayout);

        settings = getSharedPreferences("PumpItGymBuddy.AppData", 0);

        day = settings.getString("alarmday", "none");
        manualBtn.setChecked(false);
        weeklyBtn.setChecked(true);
        weeklyLL.setVisibility(View.VISIBLE);
        Log.i("PIGB", day);
        switch(day){
            case "Mon":
                monBtn.setChecked(true);
                break;
            case "Tue":
                tueBtn.setChecked(true);
                break;
            case "Wed":
                wedBtn.setChecked(true);
                break;
            case "Thu":
                thuBtn.setChecked(true);
                break;
            case "Fri":
                friBtn.setChecked(true);
                break;
            case "Sat":
                satBtn.setChecked(true);
                break;
            case "Sun":
                sunBtn.setChecked(true);
                break;
            default:
                weeklyLL.setVisibility(View.GONE);
                manualBtn.setChecked(true);
                weeklyBtn.setChecked(false);
                break;

        }

        schedFreq.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                //if we dont want reminders to do progress reports
                if (checkedId == R.id.radioButtonManual) {
                    //hide the layout altogether
                    weeklyLL.setVisibility(View.GONE);
                    day = "none";
                } else {
                    if (day.equals("none")) {
                        day = "Mon";
                        monBtn.setChecked(true);
                    }
                    weeklyLL.setVisibility(View.VISIBLE);
                }
            }
        });

        dbh = new DatabaseHelper(this);
        mListview = (AbsListView) findViewById(R.id.schedSplitListView);
        final ArrayList<Split> allSplits = new ArrayList<>(dbh.getAllSplits().values());
        allSchedules = new ArrayList<>(dbh.getAllSchedules().values());
        scheduledSplits = new Split[7];
        final ListAdapter mAdapter = new ScheduleSplitAdapter(this,scheduledSplits);



        for (Schedule s: allSchedules) {
            if (s.getSplitId() != 0) {
                switch (s.getDay()) {
                    case "Mon":
                        scheduledSplits[MON] = dbh.getSplit(s.getSplitId());
                        break;
                    case "Tue":
                        scheduledSplits[TUE] = dbh.getSplit(s.getSplitId());;
                        break;
                    case "Wed":
                        scheduledSplits[WED] = dbh.getSplit(s.getSplitId());;
                        break;
                    case "Thu":
                        scheduledSplits[THU] = dbh.getSplit(s.getSplitId());;
                        break;
                    case "Fri":
                        scheduledSplits[FRI] = dbh.getSplit(s.getSplitId());;
                        break;
                    case "Sat":
                        scheduledSplits[SAT] = dbh.getSplit(s.getSplitId());;
                        break;
                    case "Sun":
                        scheduledSplits[SUN] = dbh.getSplit(s.getSplitId());;
                        break;
                }
            }
        }

        dayGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (!manualBtn.isChecked()) {
                    switch (checkedId) {
                        case R.id.radioButtonMon:
                            day = "Mon";
                            break;
                        case R.id.radioButtonTue:
                            day = "Tue";
                            break;
                        case R.id.radioButtonWed:
                            day = "Wed";
                            break;
                        case R.id.radioButtonThu:
                            day = "Thu";
                            break;
                        case R.id.radioButtonFri:
                            day = "Fri";
                            break;
                        case R.id.radioButtonSat:
                            day = "Sat";
                            break;
                        case R.id.radioButtonSun:
                            day = "Sun";
                            break;
                    }
                }

            }
        });

        mListview.setAdapter(mAdapter);

        mListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                // get and inflate the layout xml for the dialog view
                LayoutInflater layoutInflater = LayoutInflater.from(ScheduleActivity.this);
                View dialogView = layoutInflater.inflate(R.layout.dialog_select_split_for_scheduling, null);

                if (scheduledSplits[position] == null) {
                    selectedSplit = scheduledSplits[position];
                    // setup a dialog window for displaying the split selector
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ScheduleActivity.this);
                    //apply the layout xml to the dialog
                    alertDialogBuilder.setView(dialogView);

                    final Spinner splitSplinner = (Spinner) dialogView.findViewById(R.id.selectSplitSpinner);
                    splitSplinner.setAdapter(new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, allSplits.toArray()));
                    alertDialogBuilder.setCancelable(false)
                            //set to null as we override this later
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    selectedSplit = (Split) splitSplinner.getSelectedItem();

                                    scheduledSplits[position] = selectedSplit;
                                    reloadScheduledSplits(mAdapter);

                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton("Cancel",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });


                    // create the actual alert dialog then show it
                    AlertDialog alert = alertDialogBuilder.create();
                    alert.show();
                } else {
                    // setup a dialog window to confirm unscheduling split
                    AlertDialog alertDialog = new AlertDialog.Builder(ScheduleActivity.this).create();
                    alertDialog.setTitle("Alert");
                    alertDialog.setMessage("Really unschedule split " + scheduledSplits[position].getName() + "?");
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    scheduledSplits[position] = null;
                                    reloadScheduledSplits(mAdapter);

                                }
                            });
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
            }
        });



    }

    public void reloadScheduledSplits(ListAdapter adapter) {
        mListview.setAdapter(adapter);
        mListview.invalidate();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_schedule, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_save) {
            for (int i=0;i<scheduledSplits.length;i++) {
                if (scheduledSplits[i] != null)
                    dbh.updateScheduleWithSplit(allSchedules.get(i).getDay(),scheduledSplits[i].getId());
                else
                    dbh.updateScheduleWithSplit(allSchedules.get(i).getDay(),0);
            }
            if (day.equals("none"))
                setAlarm();
            else if (!day.equals(settings.getString("alarmday", "none")))
                setAlarm();
            Toast.makeText(this,"Schedule updated",Toast.LENGTH_SHORT).show();
            Intent i = new Intent(ScheduleActivity.this, MainActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setAlarm() {
        if (!manualBtn.isChecked()) {
            Calendar calendar = Calendar.getInstance();
            //set up alarm time (6pm, passed in day of week.)
            calendar.setTimeInMillis(System.currentTimeMillis());
            //calendar.set(Calendar.DAY_OF_WEEK,Schedule.checkDay(day));
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            //get the alarm manager
            alarmMgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
            //create intent that will load AlarmReciever
            Intent intent = new Intent(ScheduleActivity.this, AlarmReceiver.class);
            //create the pending intent that will fire when alarm manager is called (the time we speicify in the calendar object)
            alarmIntent = PendingIntent.getBroadcast(ScheduleActivity.this, 0, intent, 0);
            //fire off the pending intent at the set time and day every week.
            alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                    AlarmManager.INTERVAL_FIFTEEN_MINUTES / 15, alarmIntent);
            // AlarmManager.INTERVAL_DAY * 7, alarmIntent);

            //if we are setting an alarm, detect a reboot and re-set the alarm.
            ComponentName receiver = new ComponentName(getApplicationContext(), BootReceiver.class);
            PackageManager pm = this.getPackageManager();
            pm.setComponentEnabledSetting(receiver,
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                    PackageManager.DONT_KILL_APP);
        }
        else
        {
            // If the alarm has been set, cancel it so we can set a new one based on selected day.
            Intent intent = new Intent(ScheduleActivity.this, AlarmReceiver.class);
            PendingIntent sender = PendingIntent.getBroadcast(this, 0, intent, 0);
            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

            alarmManager.cancel(sender);

            //if we are setting an alarm, detect a reboot and re-set the alarm.
            ComponentName receiver = new ComponentName(getApplicationContext(), BootReceiver.class);
            PackageManager pm = this.getPackageManager();
            pm.setComponentEnabledSetting(receiver,
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                    PackageManager.DONT_KILL_APP);

        }
        SharedPreferences.Editor editor = settings.edit();
        //save the alarm day to sharedprefs so we can retrieve it on phone reboot
        editor.putString("alarmday",day);
        editor.apply();
    }
}
