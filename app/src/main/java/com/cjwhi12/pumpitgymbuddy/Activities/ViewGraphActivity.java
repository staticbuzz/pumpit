package com.cjwhi12.pumpitgymbuddy.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cjwhi12.pumpitgymbuddy.R;
import com.cjwhi12.pumpitgymbuddy.databasehelpers.DatabaseHelper;
import com.cjwhi12.pumpitgymbuddy.models.Exercise;
import com.cjwhi12.pumpitgymbuddy.models.Measurement;
import com.cjwhi12.pumpitgymbuddy.models.WorkoutResult;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;

public class ViewGraphActivity extends Activity {

    private DatabaseHelper dbh;
    private TextView graphName;
    private GraphView graph;
    private Button prevBtn,nextBtn;
    private int pos;
    private String[] graphNames;
    private String[] graphUnits;
    private int[] graphDomainSize;
    private TextView noResults;
    private ArrayList<LineGraphSeries<DataPoint>> series;
    private ArrayList<Exercise> itemsInCategory;
    private ArrayList<?> measurements;
    private LinearLayout graphCont;
    private String category;
    private TextView title;
    private Spinner catSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_graph);

        if (getIntent().getStringExtra("category") != null)
            category = getIntent().getStringExtra("category");
        else
            category = "arms";

        title = (TextView) findViewById(R.id.viewGraphLbl);
        TextView subTitle = (TextView) findViewById(R.id.viewGraphSubLbl);

        if (category.equals("size"))
        {
            title.setText("Size Matters");
            subTitle.setText("Are you getting bigger?");
        }
        else if (category.equals("weight"))
        {
            title.setText("Weighty Considerations");
            subTitle.setText("To cut or bulk?");
        }
        else
        {
            title.setText(category.toUpperCase()+", yeah!");
            subTitle.setText("Here's your performance");
        }

        graphName = (TextView) findViewById(R.id.exerciseName);
        noResults = (TextView) findViewById(R.id.noResultsTxt);
        graph = (GraphView) findViewById(R.id.graph);

        graphCont = (LinearLayout) findViewById(R.id.graphCont);

        prevBtn = (Button) findViewById(R.id.prevBtn);
        nextBtn = (Button) findViewById(R.id.nextBtn);
        catSpinner = (Spinner) findViewById(R.id.catSpinner);

        dbh = new DatabaseHelper(this);
        dbh.deleteNullWorkoutResults();

        graphNames = null;
        graphUnits = null;
        graphDomainSize = null;

        series = new ArrayList<>();


        catSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                category = parent.getItemAtPosition(position).toString().toLowerCase();
                title.setText(category.toUpperCase()+", yeah!");
                processExerciseCategory();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        prevBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pos > 0) {
                    pos--;
                    if (series.get(pos).isEmpty()) {
                        graph.setVisibility(View.GONE);
                        noResults.setVisibility(View.VISIBLE);
                    } else {
                        graph.setVisibility(View.VISIBLE);
                        noResults.setVisibility(View.GONE);
                    }
                    graphName.setText(graphNames[pos]);
                    if (category.equals("size"))
                        graph.getGridLabelRenderer().setVerticalAxisTitle("cm");
                    else
                        graph.getGridLabelRenderer().setVerticalAxisTitle(itemsInCategory.get(pos).getUnit());
                    graph.getViewport().setXAxisBoundsManual(true);
                    graph.getViewport().setMaxX(graphDomainSize[pos]);
                    graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);
                    graph.getGridLabelRenderer().setTextSize(27);
                    graph.removeAllSeries();
                    series.get(pos).setDrawDataPoints(true);
                    series.get(pos).setDataPointsRadius(10);
                    series.get(pos).setThickness(8);
                    graph.addSeries(series.get(pos));
                    if (pos == 0) {
                        prevBtn.setEnabled(false);
                        nextBtn.setEnabled(true);
                    } else {
                        prevBtn.setEnabled(true);
                        nextBtn.setEnabled(true);
                    }
                }
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pos < graphNames.length - 1) {
                    pos++;
                    if (series.get(pos).isEmpty()) {
                        graph.setVisibility(View.GONE);
                        noResults.setVisibility(View.VISIBLE);
                    } else {
                        graph.setVisibility(View.VISIBLE);
                        noResults.setVisibility(View.GONE);
                    }
                    graphName.setText(graphNames[pos]);
                    if (category.equals("size"))
                        graph.getGridLabelRenderer().setVerticalAxisTitle("cm");
                    else
                        graph.getGridLabelRenderer().setVerticalAxisTitle(itemsInCategory.get(pos).getUnit());
                    graph.getViewport().setXAxisBoundsManual(true);
                    graph.getViewport().setMaxX(graphDomainSize[pos]);
                    graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);
                    graph.getGridLabelRenderer().setTextSize(27);
                    graph.removeAllSeries();
                    series.get(pos).setDrawDataPoints(true);
                    series.get(pos).setDataPointsRadius(10);
                    series.get(pos).setThickness(8);
                    graph.addSeries(series.get(pos));
                    if (pos == graphNames.length - 1) {
                        prevBtn.setEnabled(true);
                        nextBtn.setEnabled(false);
                    } else {
                        prevBtn.setEnabled(true);
                        nextBtn.setEnabled(true);
                    }

                }

            }
        });

        switch (category) {
            case "weight":
                graphNames = new String[1];
                graphNames[0] = "Weight";

                graphUnits = new String[1];
                graphUnits[0] = "kg";

                graphDomainSize = new int[1];


                catSpinner.setVisibility(View.GONE);
                nextBtn.setVisibility(View.GONE);
                prevBtn.setVisibility(View.GONE);

                ArrayList<?> measurements = new ArrayList<Measurement>(dbh.getMeasurementsForName("weight").values());

                DataPoint[] dataPointArr = new DataPoint[measurements.size()];
                graphDomainSize[0] = measurements.size()-1;
                for (int j = 0; j < dataPointArr.length; j++) {

                    dataPointArr[j] = new DataPoint(j, ((Measurement) measurements.get(j)).getResult());

                }
                LineGraphSeries<DataPoint> dataPoints = new LineGraphSeries<DataPoint>(dataPointArr);
                series.add(dataPoints);

                if (series.get(0).isEmpty()) {
                    graph.setVisibility(View.GONE);
                    noResults.setVisibility(View.VISIBLE);
                }
                else {
                    graph.setVisibility(View.VISIBLE);
                    noResults.setVisibility(View.GONE);
                }
                graphName.setText(graphNames[0]);
                graph.getViewport().setXAxisBoundsManual(true);
                graph.getViewport().setMaxX(graphDomainSize[0]);
                graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);
                graph.getGridLabelRenderer().setTextSize(27);
                graph.removeAllSeries();
                graph.getGridLabelRenderer().setVerticalAxisTitle(graphUnits[0]);
                series.get(pos).setDrawDataPoints(true);
                series.get(pos).setDataPointsRadius(10);
                series.get(pos).setThickness(8);
                graph.addSeries(series.get(0));
                break;

            case "size":
                processSizeCategory();

                break;
            //if category isn't weight or size then we are definitely dealing with workout results
            default:
                processExerciseCategory();
                break;
        }


    }

    @Override
    public void onResume(){
        if (category.equals("size"))
            processSizeCategory();
        else if (!category.equals("weight") && !category.equals("size"))
            processExerciseCategory();
        super.onResume();
    }

    private void processSizeCategory(){
        series = new ArrayList<>();
        pos = 0;
        String[] sizeCats = getResources().getStringArray(R.array.sizeCategories);
        graphNames = getResources().getStringArray(R.array.sizeCategoriesNice);
        graphDomainSize = new int[graphNames.length];
        graphUnits = new String[1];
        graphUnits[0] = "cm";
        catSpinner.setVisibility(View.GONE);

        for (int i = 0; i < graphNames.length; i++) {
            measurements = new ArrayList<>(dbh.getMeasurementsForName(sizeCats[i]).values());
            DataPoint[] dataPointMeasuresArr = new DataPoint[measurements.size()];
            graphDomainSize[i] = measurements.size()-1;
            for (int j = 0; j < dataPointMeasuresArr.length; j++) {
                dataPointMeasuresArr[j] = new DataPoint(j, ((Measurement) measurements.get(j)).getResult());
            }
            LineGraphSeries<DataPoint> dataPointsMeasures = new LineGraphSeries<DataPoint>(dataPointMeasuresArr);
            dataPointsMeasures.setDrawDataPoints(true);
            dataPointsMeasures.setDataPointsRadius(10);
            dataPointsMeasures.setThickness(8);
            series.add(dataPointsMeasures);
        }
        if (series.get(pos).isEmpty()) {
            graph.setVisibility(View.GONE);
            noResults.setVisibility(View.VISIBLE);
        }
        else {
            graph.setVisibility(View.VISIBLE);
            noResults.setVisibility(View.GONE);
        }
        graphName.setText(graphNames[pos]);
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMaxX(graphDomainSize[pos]);
        graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);
        graph.getGridLabelRenderer().setTextSize(27);
        graph.removeAllSeries();
        graph.getGridLabelRenderer().setVerticalAxisTitle(graphUnits[0]);

        graph.addSeries(series.get(pos));


        prevBtn.setEnabled(false);
        nextBtn.setEnabled(true);
    }

    private void processExerciseCategory(){
        series = new ArrayList<>();
        pos = 0;
        itemsInCategory = new ArrayList<>(dbh.getAllExercisesWithCategory(category).values());
        graphDomainSize = new int[itemsInCategory.size()];
        graphNames = new String[itemsInCategory.size()];
        graphUnits = new String[itemsInCategory.size()];

        for (int i = 0; i < itemsInCategory.size(); i++)
        {
            graphNames[i] = itemsInCategory.get(i).getName();
            graphUnits[i] = itemsInCategory.get(i).getUnit();
            measurements = new ArrayList<>(dbh.getWorkoutResultsForExercise(itemsInCategory.get(i).getId()).values());

            DataPoint[] dataPointArr = new DataPoint[measurements.size()];
            graphDomainSize[i] = measurements.size()-1;
            for (int j = 0; j < dataPointArr.length; j++) {

                dataPointArr[j] = new DataPoint(j, ((WorkoutResult) measurements.get(j)).getResult());

            }
            LineGraphSeries<DataPoint> dataPoints = new LineGraphSeries<DataPoint>(dataPointArr);
            dataPoints.setDrawDataPoints(true);
            dataPoints.setDataPointsRadius(10);
            dataPoints.setThickness(8);
            series.add(dataPoints);
        }
        if (series.get(pos).isEmpty()) {
            graph.setVisibility(View.GONE);
            noResults.setVisibility(View.VISIBLE);
        }
        else {
            graph.setVisibility(View.VISIBLE);
            noResults.setVisibility(View.GONE);
        }
        graphName.setText(graphNames[pos]);
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMaxX(graphDomainSize[pos]);
        graph.getGridLabelRenderer().setHorizontalLabelsVisible(false);
        graph.getGridLabelRenderer().setTextSize(27);
        graph.removeAllSeries();
        graph.getGridLabelRenderer().setVerticalAxisTitle(itemsInCategory.get(pos).getUnit());
        graph.addSeries(series.get(pos));


        prevBtn.setEnabled(false);
        nextBtn.setEnabled(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_graph, menu);
        if (getIntent().getStringExtra("category").equals("size"))
            menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.meter7white));
        else if (getIntent().getStringExtra("category").equals("weight"))
            menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.scale17white));
        else
            menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.weightliftplus));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_new) {
            if (category.equals("weight"))
            {
                // get and inflate the layout xml for the dialog view
                LayoutInflater layoutInflater = LayoutInflater.from(this);
                View dialogView = layoutInflater.inflate(R.layout.dialog_add_weight, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                //apply the layout xml to the dialog
                alertDialogBuilder.setView(dialogView);

                final EditText weightInputTxt = (EditText) dialogView.findViewById(R.id.weightInputTxt);

                // setup a dialog window for inputting results and reps
                alertDialogBuilder.setCancelable(false)
                        //set to null as we override this later
                        .setPositiveButton("OK", null)
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });


                // create the actual alert dialog then show it
                final AlertDialog alert = alertDialogBuilder.create();
                alert.show();
                weightInputTxt.requestFocus();
                //override the positive button so it doesn't immediately close before validation
                alert.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //use this to determine if we should dismiss the dialog or keep it open
                        boolean closeMe = false;

                        //make sure input is numeric or decimal
                        try {
                            dbh.addMeasurement(new Measurement("weight", "kg", Double.parseDouble(weightInputTxt.getText().toString()), dbh.getUser().getName()));
                            closeMe = true;
                            DataPoint dp = new DataPoint(graphDomainSize[0] + 1, Double.parseDouble(weightInputTxt.getText().toString()));
                            series.get(0).appendData(dp, true, 1);

                        } catch (NumberFormatException e) {
                            Toast.makeText(ViewGraphActivity.this, "Numbers only, buddy", Toast.LENGTH_SHORT).show();
                        }
                        //only dismiss on OK button press if both EditTexts contain numbers
                        if (closeMe) {
                            graph.removeAllSeries();

                            //grow the number of domain points on graph
                            graphDomainSize[0] = graphDomainSize[0] + 1;
                            graph.getViewport().setMaxX(graphDomainSize[0]);
                            //re-add the datapoint series to graph
                            graph.addSeries(series.get(0));
                            //refresh view
                            graphCont.invalidate();
                            //close dialog
                            alert.dismiss();
                        }
                    }
                });

            }
            else if (category.equals("size")) {
                Intent i = new Intent(ViewGraphActivity.this, MeasureUpActivity.class);
                startActivity(i);
            }
            else {
                Intent i = new Intent(ViewGraphActivity.this, ChooseSplitForWorkoutActivity.class);
                startActivity(i);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
