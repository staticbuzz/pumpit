package com.cjwhi12.pumpitgymbuddy.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.SimpleAdapter;

import com.cjwhi12.pumpitgymbuddy.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProgressOptionsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress);

        String[] options = getResources().getStringArray(R.array.progressOptions);
        Integer[] icons = {R.drawable.scale17,R.drawable.meter7,R.drawable.weightlifter3 ,R.drawable.camera59};

        List<Map<String, Object>> optionsMapList = new ArrayList<>();
        for(int i=0;i<options.length;i++) {
            HashMap<String, Object> option = new HashMap<>();
            option.put("option", options[i]);
            option.put("icon", icons[i]);
            optionsMapList.add(option);
        }

        AbsListView mListView = (AbsListView) findViewById(R.id.progressOptionsListView);
        SimpleAdapter mAdapter = new SimpleAdapter(this, optionsMapList,
                R.layout.list_view_pic_and_title_item,
                new String[]{"option", "", "icon"},
                new int[]{R.id.exerciseName, R.id.countVal, R.id.addRemoveSplitImg});
        mListView.setAdapter(mAdapter);

        mListView.setOnItemClickListener(progressOptionClicked);
    }

    private AdapterView.OnItemClickListener progressOptionClicked = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent i;
            switch (position) {
                case 0:
                    i = new Intent(ProgressOptionsActivity.this, ViewGraphActivity.class);
                    i.putExtra("category", "weight");
                    startActivity(i);
                    break;

                case 1:
                    i = new Intent(ProgressOptionsActivity.this, ViewGraphActivity.class);
                    i.putExtra("category", "size");
                    startActivity(i);
                    break;

                case 2:
                    i = new Intent(ProgressOptionsActivity.this, ViewGraphActivity.class);
                    i.putExtra("category", "arms");
                    startActivity(i);
                    break;

                case 3:
                    i = new Intent(ProgressOptionsActivity.this, PhotoActivity.class);
                    startActivity(i);
                    break;

            }
        }

    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_progress, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }


}
