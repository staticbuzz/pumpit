package com.cjwhi12.pumpitgymbuddy.Activities;

import android.app.Activity;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.cjwhi12.pumpitgymbuddy.ListAdapters.EditSplitAdapter;
import com.cjwhi12.pumpitgymbuddy.R;
import com.cjwhi12.pumpitgymbuddy.databasehelpers.DatabaseHelper;
import com.cjwhi12.pumpitgymbuddy.models.Exercise;
import com.cjwhi12.pumpitgymbuddy.models.Split;

import java.util.ArrayList;

public class EditSplitActivity extends Activity {

    private DatabaseHelper dbh;
    private Split createdSplit;
    private ArrayList<Exercise> exercises;
    private ListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbh = new DatabaseHelper(this);
        setContentView(R.layout.activity_edit_split);
        createdSplit = getIntent().getParcelableExtra("createdSplit");
        exercises = getIntent().getParcelableArrayListExtra("exercises");

        ArrayList<Exercise> splitExercises = new ArrayList<>(dbh.getAllExercisesForSplit(createdSplit.getId()).values());
        AbsListView mListView = (AbsListView) findViewById(R.id.exerciseSplitListView);
        mAdapter = new EditSplitAdapter(this, exercises, splitExercises,createdSplit.getId());
        mListView.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_split, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            //override the up/home actionbar btn to finish the activity and return to previous, rather than recreating prev activity. preserves data.
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_done:
                Toast.makeText(this, "Exercises saved", Toast.LENGTH_SHORT).show();
                for (int i = 0; i < exercises.size(); i++)
                {
                    dbh.removeExerciseFromSplit(exercises.get(i).getId(),createdSplit.getId());
                    if((boolean)mAdapter.getItem(i)) {
                        dbh.addExerciseToSplit(exercises.get(i).getId(), createdSplit.getId());
                    }
                }
                finish();
                return true;



        }

        return super.onOptionsItemSelected(item);
    }


}
