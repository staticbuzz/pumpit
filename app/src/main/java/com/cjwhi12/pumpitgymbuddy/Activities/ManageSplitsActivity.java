package com.cjwhi12.pumpitgymbuddy.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.cjwhi12.pumpitgymbuddy.ListAdapters.SelectSplitAdapter;
import com.cjwhi12.pumpitgymbuddy.R;
import com.cjwhi12.pumpitgymbuddy.databasehelpers.DatabaseHelper;
import com.cjwhi12.pumpitgymbuddy.models.Split;

import java.util.ArrayList;

public class ManageSplitsActivity extends Activity
{
    private ListAdapter mAdapter;
    private AbsListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_splits);

        LinearLayout noSplitsLayout = (LinearLayout) findViewById(R.id.noSplitsLayout);

        TextView countLbl = (TextView) findViewById(R.id.numSplitsTxt);
        mListView = (AbsListView) findViewById(R.id.editSplitListView);

        DatabaseHelper dbh = new DatabaseHelper(this);
        ArrayList<Split> splits = new ArrayList<>(dbh.getAllSplits().values());

        int count = dbh.getSplitsCount();
        countLbl.setText(count+"");
        if (count == 0)
        {
            mListView.setVisibility(View.GONE);
            noSplitsLayout.setVisibility(View.VISIBLE);
        }
        else
        {
            mListView.setVisibility(View.VISIBLE);
            noSplitsLayout.setVisibility(View.GONE);
        }

        mAdapter = new SelectSplitAdapter(this, splits);

        // Set the adapter
        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);



        mListView.setOnItemClickListener(editSplitClicked);
    }

    private AdapterView.OnItemClickListener editSplitClicked = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Split createdSplit = (Split)mAdapter.getItem(position);
            Intent i = new Intent(ManageSplitsActivity.this, ExerciseCategoryActivity.class);
            i.putExtra("createdSplit",createdSplit);
            startActivity(i);
        }
    };

    @Override
    public void onResume()
    {
        super.onResume();
        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);
        mListView.invalidateViews();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_manage_splits, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_add:
                Intent i = new Intent(ManageSplitsActivity.this,CreateSplitActivity.class);
                startActivity(i);

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
