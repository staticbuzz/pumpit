package com.cjwhi12.pumpitgymbuddy.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.cjwhi12.pumpitgymbuddy.ListAdapters.ExerciseCategoryListAdapter;
import com.cjwhi12.pumpitgymbuddy.R;
import com.cjwhi12.pumpitgymbuddy.databasehelpers.DatabaseHelper;
import com.cjwhi12.pumpitgymbuddy.models.Exercise;
import com.cjwhi12.pumpitgymbuddy.models.ExerciseCategory;
import com.cjwhi12.pumpitgymbuddy.models.Split;

import java.util.ArrayList;

public class ExerciseCategoryActivity extends Activity{

    public static final int ARMS_INDEX = 0;
    public static final int LEGS_INDEX = 1;
    public static final int CHEST_INDEX = 2;
    public static final int BACK_INDEX = 3;
    public static final int SHOULDERS_INDEX = 4;
    public static final int ABS_INDEX = 5;
    public static final int CARDIO_INDEX = 6;
    private ArrayList<Exercise> exercises;
    private Split createdSplit;
    private AbsListView mListView;
    private ListAdapter mAdapter;
    private EditText splitNameTxt;
    private DatabaseHelper dbh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_category);
        dbh = new DatabaseHelper(this);
        createdSplit = getIntent().getParcelableExtra("createdSplit");

        //set up the exercise categories - no need to save in db as static
        ExerciseCategory[] categories = new ExerciseCategory[7];
        categories[0] = new ExerciseCategory("Arms","biceps");
        categories[1] = new ExerciseCategory("Legs","leg");
        categories[2] = new ExerciseCategory("Chest","chest");
        categories[3] = new ExerciseCategory("Back","back");
        categories[4] = new ExerciseCategory("Shoulders","shoulders");
        categories[5] = new ExerciseCategory("Abs","abs");
        categories[6] = new ExerciseCategory("Cardio","cardio");

        mListView = (AbsListView) findViewById(R.id.exerciseCatListView);

        splitNameTxt = (EditText) findViewById(R.id.splitNameTxt);
        splitNameTxt.setText(createdSplit.getName());
        splitNameTxt.clearFocus();

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                switch (position){
                    case ARMS_INDEX:
                        exercises = new ArrayList<>(dbh.getAllExercisesWithCategory("arms").values());
                        break;
                    case LEGS_INDEX:
                        exercises = new ArrayList<>(dbh.getAllExercisesWithCategory("legs").values());
                        break;
                    case CHEST_INDEX:
                        exercises = new ArrayList<>(dbh.getAllExercisesWithCategory("chest").values());
                        break;
                    case BACK_INDEX:
                        exercises = new ArrayList<>(dbh.getAllExercisesWithCategory("back").values());
                        break;
                    case SHOULDERS_INDEX:
                        exercises = new ArrayList<>(dbh.getAllExercisesWithCategory("shoulders").values());
                        break;
                    case ABS_INDEX:
                        exercises = new ArrayList<>(dbh.getAllExercisesWithCategory("abs").values());
                        break;
                    case CARDIO_INDEX:
                        exercises = new ArrayList<>(dbh.getAllExercisesWithCategory("cardio").values());
                        break;
                }
                Intent i = new Intent(ExerciseCategoryActivity.this, EditSplitActivity.class);
                i.putExtra("createdSplit",getIntent().getParcelableExtra("createdSplit"));
                i.putParcelableArrayListExtra("exercises",exercises);
                startActivity(i);
            }
        });

        mAdapter = new ExerciseCategoryListAdapter(this, categories, createdSplit.getId());

        // Set the adapter
        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

        splitNameTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                    in.hideSoftInputFromWindow(v.getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                    // Consume the event
                    return true;

                }
                return false;
            }
        });

    }

    @Override
    public void onResume()
    {
        super.onResume();
        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_exercise_category, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_save) {
            if (!createdSplit.getName().equals(splitNameTxt.getText().toString())){
                if (splitNameTxt.getText().toString().equals("")) {
                    Toast.makeText(this, "A split's gotta have a name, buddy", Toast.LENGTH_SHORT).show();
                    splitNameTxt.requestFocus();
                }
                else
                    dbh.updateSplitName(createdSplit,splitNameTxt.getText().toString());
            }


            Intent i = new Intent(ExerciseCategoryActivity.this, ManageSplitsActivity.class);
            startActivity(i);

            return true;
        }

        if (id == R.id.action_delete) {

            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("Alert");
            alertDialog.setMessage("Really delete split " + createdSplit.getName() + "?");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dbh.removeSplit(createdSplit);
                            Toast.makeText(ExerciseCategoryActivity.this,"Split deleted",Toast.LENGTH_SHORT).show();

                            Intent i = new Intent(ExerciseCategoryActivity.this, ManageSplitsActivity.class);
                            startActivity(i);
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            return true;

        }
        return super.onOptionsItemSelected(item);
    }





}
