package com.cjwhi12.pumpitgymbuddy.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.cjwhi12.pumpitgymbuddy.R;
import com.cjwhi12.pumpitgymbuddy.databasehelpers.DatabaseHelper;
import com.cjwhi12.pumpitgymbuddy.models.Measurement;

import java.util.ArrayList;

public class MeasureUpActivity extends Activity {

    private ArrayList<EditText> inputs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measure_up);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_measure_up, menu);

        EditText bicep = (EditText) findViewById(R.id.bicepTxt);
        EditText chest = (EditText) findViewById(R.id.chestTxt);
        EditText waist = (EditText) findViewById(R.id.waistTxt);
        EditText thigh = (EditText) findViewById(R.id.thighTxt);
        EditText calf = (EditText) findViewById(R.id.calfTxt);
        EditText forearm = (EditText) findViewById(R.id.forearmTxt);

        inputs = new ArrayList<>();
        inputs.add(bicep);
        inputs.add(chest);
        inputs.add(waist);
        inputs.add(thigh);
        inputs.add(calf);
        inputs.add(forearm);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_done:
                DatabaseHelper dbh = new DatabaseHelper(this);
                String username = dbh.getUser().getName();
                String[] sizeCats = getResources().getStringArray(R.array.sizeCategories);
                int counter = 0;
                for (int i = 0; i < inputs.size(); i++) {
                    if (inputs.get(i).getText() != null && !inputs.get(i).getText().toString().equals("")) {
                        try {
                            if (dbh.addMeasurement(new Measurement(sizeCats[i], "cm", Double.parseDouble(inputs.get(i).getText().toString()), username)))
                                counter++;
                            else
                                Toast.makeText(this, "A measurement could not be saved", Toast.LENGTH_SHORT).show();
                        } catch (NumberFormatException e) {
                            Toast.makeText(this, "Numbers only, buddy", Toast.LENGTH_SHORT).show();
                            inputs.get(i).requestFocus();
                        }
                    }
                }
                if (counter == 1)
                    Toast.makeText(this, "1 measurement saved", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(this, counter + " measurements saved", Toast.LENGTH_SHORT).show();

                finish();

                return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
