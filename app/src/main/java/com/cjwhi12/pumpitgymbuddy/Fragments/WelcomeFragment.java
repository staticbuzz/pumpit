package com.cjwhi12.pumpitgymbuddy.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TableLayout;
import android.widget.TextView;


import com.cjwhi12.pumpitgymbuddy.Activities.CreateSplitActivity;
import com.cjwhi12.pumpitgymbuddy.ListAdapters.WelcomeSplitAdapter;
import com.cjwhi12.pumpitgymbuddy.R;
import com.cjwhi12.pumpitgymbuddy.databasehelpers.DatabaseHelper;
import com.cjwhi12.pumpitgymbuddy.models.Schedule;
import com.cjwhi12.pumpitgymbuddy.models.Split;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import static java.util.Calendar.DAY_OF_WEEK;

/**
 * A fragment representing a list of Items.*/

public class WelcomeFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String NAME = "param1";
    private ArrayList<Split> splits;
    private DatabaseHelper dbh;

    private String name;
    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private ListAdapter mAdapter;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public WelcomeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            name = getArguments().getString(NAME);
        }

        dbh = new DatabaseHelper(getActivity());



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                Bundle savedInstanceState){
        final View view = inflater.inflate(R.layout.fragment_welcome, container, false);
        FrameLayout fl = (FrameLayout) view.findViewById(R.id.bannerFrame);
        String name = this.getArguments().getString("name");
        LinearLayout noSplitsLayout = (LinearLayout)view.findViewById(R.id.noSplitsLayout);
        LinearLayout noLastDoneSplitLayout = (LinearLayout)view.findViewById(R.id.noLastDoneSplitLayout);
        LinearLayout noScheduledSplitLayout = (LinearLayout)view.findViewById(R.id.noScheduledSplitLayout);
        Button createSplitBtn = (Button)view.findViewById(R.id.createSplitBtn);
        TextView bannerTxt = (TextView) view.findViewById(R.id.bannerTxt);

        bannerTxt.setText("Hey there, " + name + "!\nYou pumped?");
        fl.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, 300));

        mListView = (AbsListView) view.findViewById(R.id.lastNextSplitListView);

        splits = new ArrayList<>();

         int splitsCount = dbh.getSplitsCount();
         if (splitsCount == 0)
         {
             mListView.setVisibility(View.GONE);
             noSplitsLayout.setVisibility(View.VISIBLE);
         }
         else
         {
             mListView.setVisibility(View.VISIBLE);
             noSplitsLayout.setVisibility(View.GONE);
             Split lastDoneSplit = dbh.getLastDoneSplit();
             if (lastDoneSplit != null)
             {
                 lastDoneSplit.setStatus("lastdone");
                 splits.add(lastDoneSplit);
                 noLastDoneSplitLayout.setVisibility(View.GONE);

             }
             else
                 noLastDoneSplitLayout.setVisibility(View.VISIBLE);
         }

        ArrayList<Schedule> schedules = new ArrayList<>(dbh.getAllSchedules().values());
        //SUNDAY = 0
        String day = "";
        int numDaysTillSplit = -1;
        List<ScheduleDay> scheduleDays = new ArrayList<>();
        for (int i = schedules.size()-1; i >= 0; i--)
        {
            if (schedules.get(i).getSplitId() != 0) {
                int schedDay = Schedule.checkDay(schedules.get(i).getDay());

                int tempDay = DAY_OF_WEEK + schedDay;
                if (tempDay > 6)
                    tempDay -= 7;
                numDaysTillSplit = tempDay + 1;
                if (numDaysTillSplit > 6)
                    numDaysTillSplit -= 7;

                scheduleDays.add(new ScheduleDay(schedules.get(i).getSplitId(), schedules.get(i).getDay(), numDaysTillSplit));
            }
        }
        Collections.sort(scheduleDays);

        if (scheduleDays.size() > 0) {
            if (splits.size() > 1)
                splits.remove(1);
            Split theSplit = dbh.getSplit(scheduleDays.get(0).getSplitId());
            theSplit.setStatus("scheduled");
            splits.add(theSplit);
            day = scheduleDays.get(0).getDay();
            noScheduledSplitLayout.setVisibility(View.GONE);

        }
        else {
            noScheduledSplitLayout.setVisibility(View.VISIBLE);

        }


        createSplitBtn.setOnClickListener(createSplitClicked);

        mAdapter = new WelcomeSplitAdapter(getActivity(), splits, Schedule.checkDay(day));

        // Set the adapter
        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

        return view;

    }

    private View.OnClickListener createSplitClicked = new View.OnClickListener() {
        public void onClick(View view) {
            Intent i = new Intent(getActivity(), CreateSplitActivity.class);
            startActivity(i);

        }
    };

    public static class ScheduleDay implements Comparable<ScheduleDay> {

        private long split_id;
        private String day;
        private int daysTilSplit;

        public ScheduleDay(long split_id, String day, int daysTilSplit) {
            this.split_id = split_id;
            this.daysTilSplit = daysTilSplit;
            this.day = day;
        }

        public int compareTo(ScheduleDay scheduleDay) {
            if(scheduleDay.daysTilSplit < daysTilSplit) return 1;
            if(scheduleDay.daysTilSplit > daysTilSplit) return -1;
            return 0;
        }

        public long getSplitId(){
            return split_id;
        }

        public String getDay(){
            return day;
        }
    }




}
