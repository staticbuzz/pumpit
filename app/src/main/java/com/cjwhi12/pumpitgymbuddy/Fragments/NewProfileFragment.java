package com.cjwhi12.pumpitgymbuddy.Fragments;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cjwhi12.pumpitgymbuddy.MainActivity;
import com.cjwhi12.pumpitgymbuddy.R;
import com.cjwhi12.pumpitgymbuddy.databasehelpers.DatabaseHelper;
import com.cjwhi12.pumpitgymbuddy.models.User;



/**
 * A simple {@link Fragment} subclass.
 */
public class NewProfileFragment extends Fragment{


    // no params required


    public NewProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_newprofile, container, false);
        FrameLayout fl = (FrameLayout) view.findViewById(R.id.bannerFrame);
        TextView bannerTxt = (TextView) view.findViewById(R.id.bannerTxt);

        final RadioGroup sexGroup = (RadioGroup)view.findViewById(R.id.sexGroup);
        final RadioGroup goalGroup = (RadioGroup)view.findViewById(R.id.goalGroup);

        final EditText nameTxt = (EditText)view.findViewById(R.id.nameTxt);
        final EditText heightTxt = (EditText)view.findViewById(R.id.heightTxt);
        final EditText ageTxt = (EditText)view.findViewById(R.id.ageTxt);
        final EditText weightTxt = (EditText)view.findViewById(R.id.weightTxt);

        final TextView nameLbl = (TextView)view.findViewById(R.id.nameLbl);
        final TextView ageLbl = (TextView)view.findViewById(R.id.ageLbl);
        final TextView weightLbl = (TextView)view.findViewById(R.id.weightLbl);
        final TextView heightLbl = (TextView)view.findViewById(R.id.heightLbl);

        nameLbl.requestFocus();

        Button createProfileBtn = (Button) view.findViewById(R.id.createProfileBtn);
        createProfileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String age;
                // Returns an integer which represents the selected radio button's ID
                int selectedSex = sexGroup.getCheckedRadioButtonId();
                // Gets a reference to our "selected" radio button
                RadioButton b1 = (RadioButton) view.findViewById(selectedSex);
                // Now you can get the text or whatever you want from the "selected" radio button
                b1.getText();

                // Returns an integer which represents the selected radio button's ID
                int selectedGoal = goalGroup.getCheckedRadioButtonId();
                // Gets a reference to our "selected" radio button
                RadioButton b2 = (RadioButton) view.findViewById(selectedGoal);
                // Now you can get the text or whatever you want from the "selected" radio button
                b2.getText();
                nameLbl.setTextColor(getResources().getColor(R.color.dk_gray));
                ageLbl.setTextColor(getResources().getColor(R.color.dk_gray));
                heightLbl.setTextColor(getResources().getColor(R.color.dk_gray));
                weightLbl.setTextColor(getResources().getColor(R.color.dk_gray));



                if (nameTxt.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Must enter a name", Toast.LENGTH_LONG).show();
                    nameLbl.setTextColor(Color.RED);
                } else if (ageTxt.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Must enter an age", Toast.LENGTH_LONG).show();
                    ageTxt.requestFocus();
                    ageLbl.setTextColor(Color.RED);
                } else if (weightTxt.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Must enter a weight", Toast.LENGTH_LONG).show();
                    weightTxt.requestFocus();
                    weightLbl.setTextColor(Color.RED);
                } else if (heightTxt.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Must enter a height", Toast.LENGTH_LONG).show();
                    heightTxt.requestFocus();
                    heightLbl.setTextColor(Color.RED);
                }
                //all fields contain something so now check for numeric input where required
                else {
                    try {
                        int agenum = Integer.parseInt(ageTxt.getText().toString());
                        try {
                            int weightnum = Integer.parseInt(weightTxt.getText().toString());
                            try {
                                int heightnum = Integer.parseInt(heightTxt.getText().toString());
                                //all fields full and numeric where required so save sharedpref and save user into db
                                SharedPreferences settings = getActivity().getSharedPreferences("PumpItGymBuddy.AppData", 0);
                                SharedPreferences.Editor editor = settings.edit();
                                // Add “profileExists” boolean to preferences
                                editor.putBoolean("profileExists", true);
                                editor.apply();
                                User u;

                                u = new User(
                                        nameTxt.getText().toString(),
                                        agenum,
                                        weightnum,
                                        heightnum,
                                        b1.getText().toString(),
                                        b2.getText().toString()
                                    );

                                DatabaseHelper dbh = new DatabaseHelper(getActivity());
                                if (dbh.addUser(u)) {
                                    Toast.makeText(getActivity(), "Profile Created", Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(getActivity(),MainActivity.class);
                                    startActivity(i);
                                } else {
                                    Toast.makeText(getActivity(), "Could not save profile! Try again later", Toast.LENGTH_SHORT).show();
                                }
                            } catch (NumberFormatException e) {
                                Toast.makeText(getActivity(), "Height must be numeric", Toast.LENGTH_SHORT).show();
                                heightTxt.requestFocus();
                                heightLbl.setTextColor(Color.RED);
                            }
                        } catch (NumberFormatException e) {
                            Toast.makeText(getActivity(), "Weight must be numeric", Toast.LENGTH_SHORT).show();
                            weightTxt.requestFocus();
                            weightLbl.setTextColor(Color.RED);
                        }
                    } catch (NumberFormatException e) {
                        Toast.makeText(getActivity(), "Age must be numeric", Toast.LENGTH_SHORT).show();
                        ageTxt.requestFocus();
                        ageLbl.setTextColor(Color.RED);
                    }

                }

            }
        });

        fl.setBackground(getResources().getDrawable(R.drawable.banner1));
        fl.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, 300));

        return view;

    }






}
