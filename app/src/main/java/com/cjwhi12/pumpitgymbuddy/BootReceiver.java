package com.cjwhi12.pumpitgymbuddy;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.util.Log;

import com.cjwhi12.pumpitgymbuddy.Activities.ScheduleActivity;
import com.cjwhi12.pumpitgymbuddy.models.Schedule;

import java.util.Calendar;
/*This class retrieves the calendar information chosen in the schedule activity from SharedPrefs and then
    recreates a new scheduled alarm based on that information. It does this after the phone reboots, as
    alarms are not persistent across reboots.
 */
public class BootReceiver extends BroadcastReceiver {
    public BootReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            SharedPreferences settings = context.getSharedPreferences("PumpItGymBuddy.AppData", 0);
            String day = settings.getString("alarmday", "none");
            if (!day.equals("none")) {
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(System.currentTimeMillis());
                //cal.set(Calendar.DAY_OF_WEEK, Schedule.checkDay(day));
                cal.set(Calendar.HOUR_OF_DAY, 18);
                //get the alarm manager
                AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
                //create intent that will load AlarmReciever
                Intent i = new Intent(context, AlarmReceiver.class);
                //create the pending intent that will fire when alarm manager is called (the time we speicify in the calendar object)
                PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, i, 0);
                //fire off the pending intent at the set time and day every week.
                alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                        AlarmManager.INTERVAL_FIFTEEN_MINUTES / 15, alarmIntent);
                //AlarmManager.INTERVAL_DAY * 7, alarmIntent);

            }
        }
    }
}
