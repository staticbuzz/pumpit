package com.cjwhi12.pumpitgymbuddy.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Christopher on 17/05/2015.
 */
public class Exercise implements Parcelable{
    // Database constants
    public static final String TABLE_NAME = "exercises";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_UNIT = "unit";
    public static final String COLUMN_CATEGORY = "category";



    public static final String CREATE_STATEMENT =
            "CREATE TABLE " + TABLE_NAME + "(" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    COLUMN_NAME + " TEXT NOT NULL, " +
                    COLUMN_UNIT + " TEXT NOT NULL, " +
                    COLUMN_CATEGORY + " TEXT NOT NULL " +
                    ")";

    //properties
    private long id;
    private String name;
    private String category;
    private String unit;




    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    // Default constructor with required params
    public Exercise(long id, String name, String unit, String category) {
        this.id = id;
        this.name = name;
        this.unit = unit;
        this.category = category;
    }

    public Exercise(String name, String unit, String category) {
        this.name = name;
        this.unit = unit;
        this.category = category;
    }

    //static method to make parcelable (required)
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Exercise createFromParcel(Parcel in) {
            return new Exercise(in);
        }

        public Exercise[] newArray(int size) {
            return new Exercise[size];
        }
    };


    public Exercise(Parcel in){
        this.id = in.readLong();
        this.name = in.readString();
        this.unit = in.readString();
        this.category = in.readString();
    }

    @Override
    public String toString(){
        return name;
    }

    @Override
    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i){
        parcel.writeLong(id);
        parcel.writeString(name);
        parcel.writeString(unit);
        parcel.writeString(category);
    }
}
