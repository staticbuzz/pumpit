package com.cjwhi12.pumpitgymbuddy.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;


/**
 * Created by Christopher on 17/05/2015.
 */
public class Schedule implements Parcelable {
    // Database constants
    public static final String TABLE_NAME = "schedules";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_SPLIT_ID = "split_id";
    public static final String COLUMN_DAY = "day";




    public static final String CREATE_STATEMENT =
            "CREATE TABLE " + TABLE_NAME + "(" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    COLUMN_SPLIT_ID + " INTEGER NULL, " +
                    COLUMN_DAY + " TEXT NOT NULL " +
                    ")";

    //properties
    private long id;
    private long split_id;
    private String day;


    public long getId() {
        return id;
    }

    public long getSplitId(){
        return split_id;
    }

    public void setSplitId(long split_id){
        this.split_id = split_id;
    }

    public String getDay(){
        return day;
    }

    public void setDay(String day )
    {
        this.day = day;
    }



    // Default constructor with required params
    public Schedule(long id, long split_id, String day) {
        this.id = id;
        this.split_id = split_id;
        this.day = day;
    }

    public Schedule(String day) {
        this.day = day;
        this.split_id = 0;
    }

    //static method to make parcelable (required)
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Schedule createFromParcel(Parcel in) {
            return new Schedule(in);
        }

        public Schedule[] newArray(int size) {
            return new Schedule[size];
        }
    };


    public Schedule(Parcel in){
        this.id = in.readLong();
        this.split_id = in.readLong();
        this.day = in.readString();
    }

    @Override
    public String toString(){
        return "Day: "+ day;
    }

    @Override
    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i){
        parcel.writeLong(id);
        parcel.writeLong(split_id);
        parcel.writeString(day);
    }

    public final static int checkDay(String day)
    {
        switch (day){
            case "Mon":
                return 1;
            case "Tue":
                return 2;
            case "Wed":
                return 3;
            case "Thu":
                return 4;
            case "Fri":
                return 5;
            case "Sat":
                return 6;
            case "Sun":
                return 0;
        }
        return -1;
    }
}
