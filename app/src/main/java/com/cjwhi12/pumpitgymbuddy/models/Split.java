
package com.cjwhi12.pumpitgymbuddy.models;

        import android.os.Parcel;
        import android.os.Parcelable;

        import java.util.Date;


/**
 * Created by Christopher on 12/05/2015.
 */
public class Split implements Parcelable {

    // Database constants
    public static final String TABLE_NAME = "splits";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_DATELAST = "date_last";
    public static final String COLUMN_USERNAME = "username";
    public static final String COLUMN_ICON = "icon";



    public static final String CREATE_STATEMENT =
            "CREATE TABLE " + TABLE_NAME + "(" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    COLUMN_NAME + " TEXT NOT NULL UNIQUE, " +
                    COLUMN_DATELAST + " INTEGER NULL, " +
                    COLUMN_USERNAME + " TEXT NOT NULL, " +
                    COLUMN_ICON + " TEXT NOT NULL " +
                    ")";

    //properties
    private long id;
    private String name,username,icon,status;
    private Date dateLast;


    // Default constructor with required params
    public Split(long id, String name, Date dateLast, String username, String icon) {
        this.id = id;
        this.name = name;
        this.dateLast = dateLast;
        this.username = username;
        this.icon = icon;
        this.status = null;

    }

    public Split(String name, String username, String icon) {
        this.name = name;
        this.dateLast = null;
        this.username = username;
        this.icon = icon;
    }

    //static method to make parcelable (required)
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Split createFromParcel(Parcel in) {
            return new Split(in);
        }

        public Split[] newArray(int size) {
            return new Split[size];
        }
    };



    public Split(Parcel in){
        this.id = in.readLong();
        this.name = in.readString();
        this.dateLast = (java.util.Date) in.readSerializable();
        this.username = in.readString();
        this.icon = in.readString();
    }

    public long getId() {
        return id;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateLast() {
        return dateLast;
    }

    public void setDateLast(Date dateLast) {
        this.dateLast = dateLast;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIcon(){
        return icon;
    }

    public void setIcon(String icon){
        this.icon = icon;
    }


    @Override
    public String toString(){
        return name;
    }

    @Override
    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i){
        parcel.writeLong(id);
        parcel.writeString(name);
        parcel.writeSerializable(dateLast);
        parcel.writeString(username);
        parcel.writeString(icon);

    }
}
