package com.cjwhi12.pumpitgymbuddy.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Christopher on 17/05/2015.
 */
public class Measurement implements Parcelable{
    // Database constants
    public static final String TABLE_NAME = "measurements";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_UNIT = "unit";
    public static final String COLUMN_RESULT = "result";
    public static final String COLUMN_USERNAME = "username";



    public static final String CREATE_STATEMENT =
            "CREATE TABLE " + TABLE_NAME + "(" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    COLUMN_NAME + " TEXT NOT NULL, " +
                    COLUMN_UNIT + " TEXT NOT NULL, " +
                    COLUMN_RESULT + " DOUBLE NOT NULL, " +
                    COLUMN_USERNAME + " TEXT NULL " +
                    ")";

    //properties
    private long id;
    private String name;
    private double result;
    private String unit;
    private String username;





    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getResult() {
        return result;
    }

    public void setResult(double result) {
        this.result = result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }



    // Default constructor with required params
    public Measurement(long id, String name, String unit, double result, String username) {
        this.id = id;
        this.name = name;
        this.unit = unit;
        this.result = result;
        this.username = username;
    }

    public Measurement(String name, String unit, double result, String username) {
        this.name = name;
        this.unit = unit;
        this.result = result;
        this.username = username;
    }

    //static method to make parcelable (required)
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Measurement createFromParcel(Parcel in) {
            return new Measurement(in);
        }

        public Measurement[] newArray(int size) {
            return new Measurement[size];
        }
    };


    public Measurement(Parcel in){
        this.id = in.readLong();
        this.name = in.readString();
        this.unit = in.readString();
        this.result = in.readDouble();
        this.username = in.readString();
    }

    @Override
    public String toString(){
        return name;
    }

    @Override
    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i){
        parcel.writeLong(id);
        parcel.writeString(name);
        parcel.writeString(unit);
        parcel.writeDouble(result);
        parcel.writeString(username);
    }
}
