package com.cjwhi12.pumpitgymbuddy.models;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by Christopher on 12/05/2015.
 */
public class User implements Parcelable{

    // Database constants
    public static final String TABLE_NAME = "users";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_AGE = "age";
    public static final String COLUMN_WEIGHT = "weight";
    public static final String COLUMN_HEIGHT = "height";
    public static final String COLUMN_SEX = "sex";
    public static final String COLUMN_GOAL = "goal";

    public static final String CREATE_STATEMENT =
            "CREATE TABLE " + TABLE_NAME + "(" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    COLUMN_NAME + " TEXT NOT NULL, " +
                    COLUMN_AGE + " INTEGER NOT NULL, " +
                    COLUMN_WEIGHT + " INTEGER NOT NULL, " +
                    COLUMN_HEIGHT + " INTEGER NOT NULL, " +
                    COLUMN_SEX + " TEXT NOT NULL, " +
                    COLUMN_GOAL + " TEXT NOT NULL " +
                    ")";

    //properties
    private long id;
    private String name,sex,goal;
    private int age,weight,height;

    // Default constructor with required params
    public User(long id, String name, int age, int weight, int height, String sex, String goal) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.height = height;
        this.sex = sex;
        this.goal = goal;
    }

    public User(String name, int age, int weight, int height, String sex, String goal) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.height = height;
        this.sex = sex;
        this.goal = goal;
    }

    //static method to make parcelable (required)
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public User(Parcel in){
        this.id = in.readLong();
        this.name = in.readString();
        this.age = in.readInt();
        this.weight = in.readInt();
        this.height = in.readInt();
        this.sex = in.readString();
        this.goal = in.readString();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGoal() {
        return goal;
    }

    public void setGoal(String goal) {
        this.goal = goal;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }


    @Override
    public String toString(){
        return name;
    }

    @Override
    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i){
        parcel.writeLong(id);
        parcel.writeString(name);
        parcel.writeInt(age);
        parcel.writeInt(weight);
        parcel.writeInt(height);
        parcel.writeString(sex);
        parcel.writeString(goal);
    }
}
