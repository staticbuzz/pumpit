package com.cjwhi12.pumpitgymbuddy.models;

/**
 * Created by Christopher on 26/05/2015.
 */
public class ExerciseCategory {
    private String name;

    public String getName() {
        return name;
    }

    private String icon;

    public String getIcon() {
        return icon;
    }

    public ExerciseCategory(String name, String icon){
        this.name = name;
        this.icon = icon;
    }

}
