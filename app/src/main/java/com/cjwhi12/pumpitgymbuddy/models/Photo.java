package com.cjwhi12.pumpitgymbuddy.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;


/**
 * Created by Christopher on 17/05/2015.
 */
public class Photo implements Parcelable {
    // Database constants
    public static final String TABLE_NAME = "photos";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_FILEPATH = "filepath";
    public static final String COLUMN_DATE_TAKEN = "date_taken";
    public static final String COLUMN_USERNAME = "username";




    public static final String CREATE_STATEMENT =
            "CREATE TABLE " + TABLE_NAME + "(" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    COLUMN_FILEPATH + " TEXT NOT NULL, " +
                    COLUMN_DATE_TAKEN + " INTEGER NOT NULL, " +
                    COLUMN_USERNAME + " TEXT NOT NULL" +
                    ")";

    //properties
    private long id;
    private String filepath;
    private Date dateTaken;
    private String username;


    public long getId() {
        return id;
    }

    public String getFilePath(){
        return filepath;
    }

    public void setFilepath(String filepath){
        this.filepath = filepath;
    }

    public Date getDateTaken(){
        return dateTaken;
    }

    public void setDateTaken(Date dateTaken )
    {
        this.dateTaken = dateTaken;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    // Default constructor with required params
    public Photo(long id, String filepath, Date dateTaken, String username) {
        this.id = id;
        this.filepath = filepath;
        this.dateTaken = dateTaken;
        this.username = username;
    }

    public Photo(String filepath, Date dateTaken, String username) {
        this.filepath = filepath;
        this.dateTaken = dateTaken;
        this.username = username;
    }

    //static method to make parcelable (required)
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };


    public Photo(Parcel in){
        this.id = in.readLong();
        this.filepath = in.readString();
        this.dateTaken = (java.util.Date) in.readSerializable();
        this.username = in.readString();
    }

    @Override
    public String toString(){
        return "Photo filepath: "+ filepath;
    }

    @Override
    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i){
        parcel.writeLong(id);
        parcel.writeString(filepath);
        parcel.writeSerializable(dateTaken);
        parcel.writeString(username);

    }
}
