package com.cjwhi12.pumpitgymbuddy.models;

import android.graphics.drawable.Drawable;

/**
 * Created by Christopher on 5/05/2015.
 */
public class Action {

    private String title;
    private Drawable actionImageId;

    public Action(String title, Drawable actionImageId) {
        this.title = title;
        this.actionImageId = actionImageId;
    }

    public Action(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getActionImageId() {
        return actionImageId;
    }

    public void setActionImage(Drawable actionImageId) {
        this.actionImageId = actionImageId;
    }


}
