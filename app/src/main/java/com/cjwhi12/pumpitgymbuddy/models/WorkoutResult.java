package com.cjwhi12.pumpitgymbuddy.models;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by Christopher on 17/05/2015.
 */
public class WorkoutResult implements Parcelable {
    // Database constants
    public static final String TABLE_NAME = "workouts";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_EXERCISE_ID = "exercise_id";
    public static final String COLUMN_SPLIT_ID = "split_id";
    public static final String COLUMN_RESULT = "result";
    public static final String COLUMN_REPS = "reps";
    public static final String COLUMN_USERNAME = "username";




    public static final String CREATE_STATEMENT =
            "CREATE TABLE " + TABLE_NAME + "(" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    COLUMN_EXERCISE_ID + " INTEGER NOT NULL, " +
                    COLUMN_SPLIT_ID + " INTEGER NOT NULL, " +
                    COLUMN_RESULT + " DOUBLE NULL DEFAULT NULL, " +
                    COLUMN_REPS + " INTEGER NULL DEFAULT NULL, " +
                    COLUMN_USERNAME + " TEXT NOT NULL " +
                    ")";

    //properties
    private long id;
    private long exercise_id;
    private long split_id;
    private double result;
    private int reps;
    private String username;





    public long getId() {
        return id;
    }

    public long getExerciseId() {
        return exercise_id;
    }

    public long getSplitId() {
        return split_id;
    }

    public double getResult() {
        return result;
    }

    public int getReps() {
        return reps;
    }

    public void setResult(double result) {
        this.result = result;
    }

    public void setReps(int reps) {
        this.reps = reps;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    // Default constructor with required params
    public WorkoutResult(long id, long exercise_id, long split_id, double result, int reps, String username) {
        this.id = id;
        this.exercise_id = exercise_id;
        this.split_id = split_id;
        this.result = result;
        this.reps = reps;
        this.username = username;
    }

    public WorkoutResult(long exercise_id, long split_id, double result, int reps, String username) {
        this.exercise_id = exercise_id;
        this.split_id = split_id;
        this.result = result;
        this.reps = reps;
        this.username = username;

    }

    //static method to make parcelable (required)
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public WorkoutResult createFromParcel(Parcel in) {
            return new WorkoutResult(in);
        }

        public WorkoutResult[] newArray(int size) {
            return new WorkoutResult[size];
        }
    };


    public WorkoutResult(Parcel in){
        this.id = in.readLong();
        this.exercise_id = in.readLong();
        this.split_id = in.readLong();
        this.result = in.readDouble();
        this.reps = in.readInt();
        this.username = in.readString();

    }

    @Override
    public String toString(){
        return "Workout Result: "+exercise_id;
    }

    @Override
    public int describeContents(){
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i){
        parcel.writeLong(id);
        parcel.writeLong(exercise_id);
        parcel.writeLong(split_id);
        parcel.writeDouble(result);
        parcel.writeInt(reps);
        parcel.writeString(username);

    }
}
