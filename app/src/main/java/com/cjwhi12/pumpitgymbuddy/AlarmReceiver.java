package com.cjwhi12.pumpitgymbuddy;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmReceiver extends BroadcastReceiver {
    public AlarmReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent alarmService = new Intent(context, AlarmService.class);
        context.startService(alarmService);
        // an Intent broadcast.
        //throw new UnsupportedOperationException("Not yet implemented");
    }
}
