package com.cjwhi12.pumpitgymbuddy;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;

public class AlarmService extends Service {

    private NotificationManager notificationManager;

    public AlarmService() {}

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        //throw new UnsupportedOperationException("Not yet implemented");
        return null;
    }


    @Override
    public void onStart(Intent intent, int startId)
    {
        super.onStart(intent, startId);

        notificationManager = (NotificationManager) this.getApplicationContext().getSystemService(this.getApplicationContext().NOTIFICATION_SERVICE);
        Intent i = new Intent(this.getApplicationContext(),MainActivity.class);

        //Notification notification1 = new Notification(R.drawable.thumbs50white,"It's time to record your measurements!", System.currentTimeMillis());
        //i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP| Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Notification notification = new Notification.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.weightlift50)
                .setContentText("It's time to do your progress report")
                .setContentTitle("Measure Up!")
                .setColor(Color.argb(255,0,0,255))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .build();


        PendingIntent pendingNotificationIntent = PendingIntent.getActivity( this.getApplicationContext(),0, i,PendingIntent.FLAG_UPDATE_CURRENT);
        //notification1.flags |= Notification.FLAG_AUTO_CANCEL;
        //notification1.setLatestEventInfo(this.getApplicationContext(), "AlarmManagerDemo", "This is a test message!", pendingNotificationIntent);

        notificationManager.notify(0, notification);
    }

}
