package com.cjwhi12.pumpitgymbuddy;

import android.content.Context;
import android.view.View;

import com.cjwhi12.pumpitgymbuddy.models.Action;

/**
 * Created by Christopher on 30/05/2015.
 */
public class ActionsHolder {
    private Action[] actions;

    public ActionsHolder(Context context){
        actions = new Action[5];
        Action homeAction = new Action(context.getResources().getString(R.string.home),context.getResources().getDrawable( R.drawable.home153 ));
        actions[0] = homeAction;
        Action workoutAction = new Action(context.getResources().getString(R.string.workoutnow), context.getResources().getDrawable(R.drawable.weightlifter3));
        actions[1] = workoutAction;
        Action splitAction = new Action(context.getResources().getString(R.string.managesplits),context.getResources().getDrawable( R.drawable.split4 ));
        actions[2] = splitAction;
        Action progressAction = new Action(context.getResources().getString(R.string.progress),context.getResources().getDrawable( R.drawable.meter7 ));
        actions[3] = progressAction;
        Action scheduleAction = new Action(context.getResources().getString(R.string.scheduling),context.getResources().getDrawable(R.drawable.schedule2));
        actions[4] = scheduleAction;
    }

    public Action[] getActions(){
        return actions;
    }
}
