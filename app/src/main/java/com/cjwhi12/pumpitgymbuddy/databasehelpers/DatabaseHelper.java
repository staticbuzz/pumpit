package com.cjwhi12.pumpitgymbuddy.databasehelpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.cjwhi12.pumpitgymbuddy.R;
import com.cjwhi12.pumpitgymbuddy.models.Exercise;
import com.cjwhi12.pumpitgymbuddy.models.Measurement;
import com.cjwhi12.pumpitgymbuddy.models.Photo;
import com.cjwhi12.pumpitgymbuddy.models.Schedule;
import com.cjwhi12.pumpitgymbuddy.models.Split;
import com.cjwhi12.pumpitgymbuddy.models.User;
import com.cjwhi12.pumpitgymbuddy.models.WorkoutResult;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Christopher on 26/05/2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    // Set database properties
    public static final String DATABASE_NAME = "PumpItDB";
    public static final int DATABASE_VERSION = 1;

    //put this here as we don't really have any need for a exercise_split model
    public static final String EXERCISE_SPLIT_TABLE_NAME = "EXERCISE_SPLIT";
    public static final String CREATE_EXERCISE_SPLIT_STATEMENT =
            "CREATE TABLE " + EXERCISE_SPLIT_TABLE_NAME + "(" +
                    "ID" + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "EXERCISE_ID" + " INTEGER NOT NULL, " +
                    "SPLIT_ID" + " INTEGER NOT NULL " +
                    ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        // creating required tables
        db.execSQL(User.CREATE_STATEMENT);
        db.execSQL(Split.CREATE_STATEMENT);
        db.execSQL(Exercise.CREATE_STATEMENT);
        db.execSQL(WorkoutResult.CREATE_STATEMENT);
        db.execSQL(CREATE_EXERCISE_SPLIT_STATEMENT);
        db.execSQL(Measurement.CREATE_STATEMENT);
        db.execSQL(Photo.CREATE_STATEMENT);
        db.execSQL(Schedule.CREATE_STATEMENT);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + User.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Split.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Exercise.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + WorkoutResult.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + EXERCISE_SPLIT_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Measurement.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Photo.TABLE_NAME);

        // create new tables
        onCreate(db);
    }

    // closing database
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    //Users table methods

    public boolean addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(User.COLUMN_NAME, user.getName());
        values.put(User.COLUMN_AGE, user.getAge());
        values.put(User.COLUMN_WEIGHT, user.getWeight());
        values.put(User.COLUMN_HEIGHT, user.getHeight());
        values.put(User.COLUMN_SEX, user.getSex());
        values.put(User.COLUMN_GOAL, user.getGoal());
        if (db.insert(User.TABLE_NAME, null, values) != 0) {
            closeDB();
            return true;
        }
        else
        {
            closeDB();
            return false;
        }
    }

    public User getUser()  {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + User.TABLE_NAME, null);
        User user = null;
        // Add reminder to hash map for each row result
        if(cursor.moveToFirst()) {
            user = new User(cursor.getLong(0), cursor.getString(1), cursor.getInt(2),
                    cursor.getInt(3),cursor.getInt(4),cursor.getString(5),cursor.getString(6));
        }
        // Close cursor
        cursor.close();
        closeDB();
        return user;
    }

    public void removeUser() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(User.TABLE_NAME, null, null);
        closeDB();
    }

    //Splits table methods
    public boolean addSplit(Split split) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Split.COLUMN_NAME, split.getName());
        if (split.getDateLast() != null)
            values.put(Split.COLUMN_DATELAST, split.getDateLast().getTime());
        values.put(Split.COLUMN_USERNAME, split.getUsername());
        values.put(Split.COLUMN_ICON, split.getIcon());



        if (db.insert(Split.TABLE_NAME, null, values) != -1) {
            closeDB();
            return true;
        }
        else
        {
            closeDB();
            return false;
        }
    }



    public HashMap<Long, Split> getAllSplits()  {
        HashMap<Long, Split> splits = new LinkedHashMap<Long, Split>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Split.TABLE_NAME, null);
        // Add split to hash map for each row result
        if(cursor.moveToFirst()) {
            do {
                Split s = new Split(
                        cursor.getLong(0),
                        cursor.getString(1),
                        new Date(cursor.getLong(2)),
                        cursor.getString(3),
                        cursor.getString(4));
                splits.put(s.getId(), s);
            } while (cursor.moveToNext());
        }
        // Close cursor
        cursor.close();
        closeDB();
        return splits;
    }

    public Split getSplit(long id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Split.TABLE_NAME + " WHERE id = " + id, null);

        Split split = null;

        if(cursor.moveToFirst()) {
            Date dateLong;
            if (cursor.getLong(2) > 0)
            {
                dateLong = new Date(cursor.getLong(2));
            }
            else
                dateLong = null;

            split = new Split(cursor.getLong(0),
                    cursor.getString(1),
                    dateLong,
                    cursor.getString(3),
                    cursor.getString(4)
            );
        }
        // Close cursor
        cursor.close();
        closeDB();
        return split;
    }

    public Split getNewlyCreatedSplit(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Split.TABLE_NAME + " WHERE id = (select max(id) from " + Split.TABLE_NAME + ")", null);

        Split split = null;

        if(cursor.moveToFirst()) {
            Date dateLong;
            if (cursor.getLong(2) > 0)
            {
                dateLong = new Date(cursor.getLong(2));
            }
            else
                dateLong = null;


            split = new Split(cursor.getLong(0),
                    cursor.getString(1),
                    dateLong,
                    cursor.getString(3),
                    cursor.getString(4)
            );
        }
        // Close cursor
        cursor.close();
        closeDB();
        return split;
    }

    public boolean removeSplit(Split split) {
        SQLiteDatabase db = this.getWritableDatabase();
        String where = "id=?";
        String[] whereArgs = new String[] {String.valueOf(split.getId())};
        if (db.delete(Split.TABLE_NAME, where, whereArgs) != -1)
        {
            db.close();
            return true;
        }
        else
        {
           db.close();
            return false;
        }
    }

    public int getSplitsCount(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Split.TABLE_NAME, null);
        int cnt = cursor.getCount();
        cursor.close();
        closeDB();
        return cnt;
    }



    //Exercises table methods
    public boolean addExercise(Exercise exercise) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Exercise.COLUMN_NAME, exercise.getName());
        values.put(Exercise.COLUMN_UNIT, exercise.getUnit());
        values.put(Exercise.COLUMN_CATEGORY, exercise.getCategory());

        if (db.insert(Exercise.TABLE_NAME, null, values) != -1) {
            closeDB();
            return true;
        }
        else
        {
            closeDB();
            return false;
        }
    }


    public HashMap<Long, Exercise> getAllExercises()  {
        HashMap<Long, Exercise> exercises = new LinkedHashMap<Long, Exercise>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Exercise.TABLE_NAME, null);
        // Add split to hash map for each row result
        if(cursor.moveToFirst()) {
            do {
                Exercise e = new Exercise(
                        cursor.getLong(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3)
                );
                exercises.put(e.getId(), e);
            } while (cursor.moveToNext());
        }
        // Close cursor
        cursor.close();
        closeDB();

        return exercises;
    }

    public int getExerciseInSplitCountForCategory(String category,long split_id){
        HashMap<Long, Exercise> exercises = new LinkedHashMap<Long, Exercise>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT ex.* " +
                "FROM " + Exercise.TABLE_NAME + " ex " +
                "INNER JOIN " + EXERCISE_SPLIT_TABLE_NAME + " ex_sp ON ex.id=ex_sp.exercise_id " +
                "WHERE ex_sp.split_id = ? AND ex.category = ?", new String[]{String.valueOf(split_id), category});
        // Add split to hash map for each row result
        int cnt = cursor.getCount();
        cursor.close();
        closeDB();
        return cnt;
    }

    public HashMap<Long, Exercise> getAllExercisesWithCategory(String category)  {
        HashMap<Long, Exercise> exercises = new LinkedHashMap<Long, Exercise>();
        SQLiteDatabase db = this.getReadableDatabase();
        String where = "category=?";
        String[] whereArgs = new String[] {category};
        Cursor cursor = db.query(Exercise.TABLE_NAME, null, where, whereArgs, null, null, null);
        // Add split to hash map for each row result
        if(cursor.moveToFirst()) {
            do {
                Exercise e = new Exercise(
                        cursor.getLong(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3)
                );
                exercises.put(e.getId(), e);

            } while (cursor.moveToNext());
        }
        // Close cursor
        cursor.close();
        closeDB();

        return exercises;
    }


    public Exercise getExercise(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Exercise.TABLE_NAME + " WHERE id = " + id, null);

        Exercise exercise = null;

        if(cursor.moveToFirst()) {

            exercise = new Exercise(
                    cursor.getLong(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3)
            );
        }
        // Close cursor
        cursor.close();
        closeDB();
        return exercise;
    }

    public void removeExercise(Exercise exercise) {
        SQLiteDatabase db = this.getWritableDatabase();
        String where = "id=?";
        String[] whereArgs = new String[] {String.valueOf(exercise.getId())};
        db.delete(Exercise.TABLE_NAME, where, whereArgs);
        closeDB();
    }

    public int getExercisesCount(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Exercise.TABLE_NAME, null);
        int cnt = cursor.getCount();
        cursor.close();
        closeDB();
        return cnt;
    }

    public HashMap<Long, Exercise> getAllExercisesForSplit(long split_id)  {
        HashMap<Long, Exercise> exercises = new LinkedHashMap<Long, Exercise>();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT ex.* " +
                "FROM " + Exercise.TABLE_NAME + " ex " +
                "INNER JOIN " + EXERCISE_SPLIT_TABLE_NAME + " ex_sp ON ex.id=ex_sp.exercise_id " +
                "WHERE ex_sp.split_id = ?", new String[]{String.valueOf(split_id)});
        // Add split to hash map for each row result
        if(cursor.moveToFirst()) {
            do {
                Exercise e = new Exercise(
                        cursor.getLong(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3)
                );
                exercises.put(e.getId(), e);
            }
            while (cursor.moveToNext());
        }
        // Close cursor
        cursor.close();
        closeDB();
        return exercises;
    }

    public int getExerciseCountForSplit(long split_id){
        SQLiteDatabase db = this.getReadableDatabase();
        String where = "split_id=?";
        String[] whereArgs = new String[] {String.valueOf(split_id)};
        Cursor cursor = db.query(EXERCISE_SPLIT_TABLE_NAME, null, where, whereArgs, null, null, null);
        int cnt = cursor.getCount();
        cursor.close();
        closeDB();
        return cnt;
    }

    public boolean addExerciseToSplit(long exercise_id,long split_id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("exercise_id", exercise_id);
        values.put("split_id", split_id);
        if (db.insert(EXERCISE_SPLIT_TABLE_NAME, null, values) != -1) {
            closeDB();
            return true;
        }
        else
        {
            closeDB();
            return false;
        }
    }

    public boolean removeExerciseFromSplit(long exercise_id,long split_id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String where = "exercise_id=? AND split_id=?";
        String[] whereArgs = new String[] {String.valueOf(exercise_id),String.valueOf(split_id)};
        if (db.delete(EXERCISE_SPLIT_TABLE_NAME, where, whereArgs) == 0)
        {
            db.close();
            return true;
        }
        else
        {
            db.close();
            return false;
        }
    }

    //WorkoutResults table methods

    public boolean addWorkoutResult(WorkoutResult wr) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(WorkoutResult.COLUMN_EXERCISE_ID, wr.getExerciseId());
        values.put(WorkoutResult.COLUMN_SPLIT_ID, wr.getSplitId());
        values.put(WorkoutResult.COLUMN_RESULT, wr.getResult());
        values.put(WorkoutResult.COLUMN_REPS, wr.getReps());
        values.put(WorkoutResult.COLUMN_USERNAME, wr.getUsername());

        if (db.insert(WorkoutResult.TABLE_NAME, null, values) != -1) {
            closeDB();
            return true;
        }
        else
        {
            closeDB();
            return false;
        }
    }

    public boolean updateSplit(Split s) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Split.COLUMN_DATELAST, s.getDateLast().getTime());
        String where = "id=?";
        String[] whereArgs = new String[] {String.valueOf(s.getId())};
        if (db.update(Split.TABLE_NAME, values, where, whereArgs) != -1) {
            closeDB();
            return true;
        }
        else
        {
            closeDB();
            return false;
        }
    }

    public boolean updateSplitName(Split s,String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Split.COLUMN_NAME, name);
        String where = "id=?";
        String[] whereArgs = new String[] {String.valueOf(s.getId())};
        if (db.update(Split.TABLE_NAME, values, where, whereArgs) != -1) {
            closeDB();
            return true;
        }
        else
        {
            closeDB();
            return false;
        }
    }

    public Split getLastDoneSplit() {
        Split split = null;
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * " +
                "FROM " + Split.TABLE_NAME +
                " WHERE date_last = (select max(date_last) from splits)", null);
        if (cursor.moveToFirst()) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(cursor.getLong(2));
            Date lastDone = calendar.getTime();
            split = new Split(cursor.getLong(0),
                    cursor.getString(1),
                    lastDone,
                    cursor.getString(3),
                    cursor.getString(4));
        }

        cursor.close();
        closeDB();
        return split;
    }


    public HashMap<Long, WorkoutResult> getAllWorkoutResults()  {
        HashMap<Long, WorkoutResult> workoutResults = new LinkedHashMap<Long, WorkoutResult>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + WorkoutResult.TABLE_NAME, null);
        // Add split to hash map for each row result
        if(cursor.moveToFirst()) {
            do {
                WorkoutResult wr = new WorkoutResult(
                        cursor.getLong(0),
                        cursor.getLong(1),
                        cursor.getLong(2),
                        cursor.getDouble(3),
                        cursor.getInt(4),
                        cursor.getString(5)
                );
                workoutResults.put(wr.getId(), wr);
            } while (cursor.moveToNext());
        }
        // Close cursor
        cursor.close();
        closeDB();

        return workoutResults;
    }



    public HashMap<Long, WorkoutResult> getAllWorkoutResultsForExerciseCategory(String category)  {
        HashMap<Long, WorkoutResult> results = new LinkedHashMap<Long, WorkoutResult>();
        SQLiteDatabase db = this.getReadableDatabase();
        WorkoutResult wr;
        Cursor cursor = db.rawQuery("SELECT wr.* " +
                "FROM " + WorkoutResult.TABLE_NAME + " wr " +
                "INNER JOIN " + Exercise.TABLE_NAME + " ex ON ex.id=wr.exercise_id " +
                "WHERE ex.category = ?", new String[]{String.valueOf(category)});
        // Add workoutresult to hash map for each row result
        if(cursor.moveToFirst()) {
            do {
                wr = new WorkoutResult(
                        cursor.getLong(0),
                        cursor.getLong(1),
                        cursor.getLong(2),
                        cursor.getDouble(3),
                        cursor.getInt(4),
                        cursor.getString(5)
                );
                results.put(wr.getId(), wr);
            }
            while (cursor.moveToNext());
        }
        // Close cursor
        cursor.close();
        closeDB();
        return results;
    }

    public WorkoutResult getLastWorkoutResultForExercise(long exercise_id)  {
        WorkoutResult wr = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String where = "id=(select max(id) from "+ WorkoutResult.TABLE_NAME + " where exercise_id=?)";
        String[] whereArgs = new String[] {String.valueOf(exercise_id)};
        Cursor cursor = db.query(WorkoutResult.TABLE_NAME, null, where, whereArgs, null, null, null);
        // Add split to hash map for each row result
        if(cursor.moveToFirst()) {
            wr = new WorkoutResult(
                    cursor.getLong(0),
                    cursor.getLong(1),
                    cursor.getLong(2),
                    cursor.getDouble(3),
                    cursor.getInt(4),
                    cursor.getString(5)
            );
        }
        // Close cursor
        cursor.close();
        closeDB();
        return wr;
    }

    public Map<Long, WorkoutResult> getWorkoutResultsForExercise(long exercise_id){
        SQLiteDatabase db = this.getReadableDatabase();
        WorkoutResult wr;
        HashMap<Long, WorkoutResult> results = new HashMap<>();
        String where = "exercise_id=?";
        String orderby = WorkoutResult.COLUMN_ID + " ASC";
        String[] whereArgs = new String[] {String.valueOf(exercise_id)};
        Cursor cursor = db.query(WorkoutResult.TABLE_NAME, null, where, whereArgs, null, null, orderby);
        if(cursor.moveToFirst()) {
            do {
                wr = new WorkoutResult(
                        cursor.getLong(0),
                        cursor.getLong(1),
                        cursor.getLong(2),
                        cursor.getDouble(3),
                        cursor.getInt(4),
                        cursor.getString(5)
                );
                results.put(wr.getId(), wr);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        closeDB();
        Map<Long, WorkoutResult> sorted = new TreeMap<Long, WorkoutResult>(
                new Comparator<Long>() {

                    @Override
                    public int compare(Long o1, Long o2) {
                        return o1.compareTo(o2);
                    }

                });
        sorted.putAll(results);
        return sorted;
    }



    public int getWorkoutResultsCountForSplit(long split_id){
        SQLiteDatabase db = this.getReadableDatabase();
        String where = "split_id=?";
        String[] whereArgs = new String[] {String.valueOf(split_id)};
        Cursor cursor = db.query(WorkoutResult.TABLE_NAME, null, where, whereArgs, null, null, null);
        int cnt = cursor.getCount();
        cursor.close();
        closeDB();
        return cnt;
    }

    public void deleteNullWorkoutResults(){
        SQLiteDatabase db = this.getWritableDatabase();
        String where = "reps=? and result=?";
        String[] whereArgs = new String[] {String.valueOf(0),String.valueOf(0.0)};
        db.delete(WorkoutResult.TABLE_NAME, where, whereArgs);
        closeDB();
    }

    public void deleteWorkouts(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(WorkoutResult.TABLE_NAME, null, null);
        closeDB();
    }

    public boolean addMeasurement(Measurement m) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Measurement.COLUMN_NAME, m.getName());
        values.put(Measurement.COLUMN_UNIT, m.getUnit());
        values.put(Measurement.COLUMN_RESULT, m.getResult());
        values.put(Measurement.COLUMN_USERNAME, m.getUsername());

        if (db.insert(Measurement.TABLE_NAME, null, values) != -1) {
            closeDB();
            return true;
        }
        else
        {
            closeDB();
            return false;
        }
    }

    public Map<Long, Measurement> getMeasurementsForName(String name){
        SQLiteDatabase db = this.getReadableDatabase();
        Measurement m;
        HashMap<Long, Measurement> results = new HashMap<>();
        String where = "name=?";
        String orderby = WorkoutResult.COLUMN_ID + " ASC";
        String[] whereArgs = new String[] {name};
        Cursor cursor = db.query(Measurement.TABLE_NAME, null, where, whereArgs, null, null, orderby);
        if(cursor.moveToFirst()) {
            do {
                m = new Measurement(
                        cursor.getLong(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getDouble(3),
                        cursor.getString(4)
                );
                results.put(m.getId(), m);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        closeDB();
        Map<Long, Measurement> sorted = new TreeMap<Long, Measurement>(
                new Comparator<Long>() {

                    @Override
                    public int compare(Long o1, Long o2) {
                        return o1.compareTo(o2);
                    }

                });
        sorted.putAll(results);
        return sorted;
    }

    public Map<Long, Measurement> getAllMeasurements(){
        SQLiteDatabase db = this.getReadableDatabase();
        Measurement m;
        HashMap<Long, Measurement> results = new HashMap<>();
        String orderby = WorkoutResult.COLUMN_ID + " ASC";
        Cursor cursor = db.query(Measurement.TABLE_NAME, null, null, null, null, null, orderby);
        if(cursor.moveToFirst()) {
            do {
                m = new Measurement(
                        cursor.getLong(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getDouble(3),
                        cursor.getString(4)
                );
                results.put(m.getId(), m);
            }
            while (cursor.moveToNext());
        }
        cursor.close();
        closeDB();
        Map<Long, Measurement> sorted = new TreeMap<Long, Measurement>(
                new Comparator<Long>() {

                    @Override
                    public int compare(Long o1, Long o2) {
                        return o1.compareTo(o2);
                    }

                });
        sorted.putAll(results);
        return sorted;
    }

    public boolean addPhoto(Photo p){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Photo.COLUMN_FILEPATH, p.getFilePath());
        values.put(Photo.COLUMN_DATE_TAKEN, p.getDateTaken().getTime());
        values.put(Photo.COLUMN_USERNAME, p.getUsername());

        if (db.insert(Photo.TABLE_NAME, null, values) != -1) {
            closeDB();
            return true;
        }
        else
        {
            closeDB();
            return false;
        }
    }

    public Photo getInitialPhoto(){
        SQLiteDatabase db = this.getReadableDatabase();
        Photo p = null;
        String where = "  date_taken = (select min(date_taken) from photos)";
        Cursor cursor = db.query(Photo.TABLE_NAME, null, where, null, null, null, null);
        if(cursor.moveToFirst()) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(cursor.getLong(2));
            Date dateTaken = calendar.getTime();
                p = new Photo(
                        cursor.getLong(0),
                        cursor.getString(1),
                        dateTaken,
                        cursor.getString(3)
                );
        }
        cursor.close();
        closeDB();

        return p;
    }

    public Photo getLatestPhoto(){
        SQLiteDatabase db = this.getReadableDatabase();
        Photo p = null;
        String where = " date_taken = (select max(date_taken) from photos)";
        Cursor cursor = db.query(Photo.TABLE_NAME, null, where, null, null, null, null);
        if(cursor.moveToFirst()) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(cursor.getLong(2));
            Date dateTaken = calendar.getTime();
            p = new Photo(
                    cursor.getLong(0),
                    cursor.getString(1),
                    dateTaken,
                    cursor.getString(3)
            );
        }
        cursor.close();
        closeDB();

        return p;
    }

    public int getPhotosCount(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Photo.TABLE_NAME, null, null, null, null, null, null);
        int cnt = cursor.getCount();
        cursor.close();
        closeDB();
        return cnt;
    }

    //Schedule table methods
    public boolean addSchedule(Schedule schedule) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Schedule.COLUMN_DAY, schedule.getDay());

        if (db.insert(Schedule.TABLE_NAME, null, values) != -1) {
            closeDB();
            return true;
        }
        else
        {
            closeDB();
            return false;
        }
    }

    public int getScheduleCount(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Schedule.TABLE_NAME, null);
        int cnt = cursor.getCount();
        cursor.close();
        closeDB();
        return cnt;
    }

    public void updateScheduleWithSplit(String day, long split_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        String where = "day=?";
        String[] whereArgs = new String[] {day};
        values.put(Schedule.COLUMN_SPLIT_ID, split_id);
        db.update(Schedule.TABLE_NAME, values, where, whereArgs);
        closeDB();
    }

    public HashMap<Long, Schedule> getAllSchedules()  {
        HashMap<Long, Schedule> schedules = new LinkedHashMap<Long, Schedule>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Schedule.TABLE_NAME, null);
        // Add split to hash map for each row result
        if(cursor.moveToFirst()) {
            do {
                Schedule s = new Schedule(
                        cursor.getLong(0),
                        cursor.getLong(1),
                        cursor.getString(2));
                schedules.put(s.getId(), s);
            } while (cursor.moveToNext());
        }
        // Close cursor
        cursor.close();
        closeDB();
        Map<Long, Measurement> sorted = new TreeMap<Long, Measurement>(
                new Comparator<Long>() {

                    @Override
                    public int compare(Long o1, Long o2) {
                        return o1.compareTo(o2);
                    }

                });
        sorted.putAll(sorted);
        return schedules;
    }


}
