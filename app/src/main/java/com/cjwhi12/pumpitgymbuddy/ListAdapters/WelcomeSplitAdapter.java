

package com.cjwhi12.pumpitgymbuddy.ListAdapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cjwhi12.pumpitgymbuddy.R;
import com.cjwhi12.pumpitgymbuddy.models.Split;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Christopher on 16/04/2015.
 */
public class WelcomeSplitAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Split> splits;
    private int day;

    public WelcomeSplitAdapter(Context context, ArrayList<Split> splits,int day) {
        this.context = context;
        this.splits = splits;
        this.day = day;
    }

    @Override
    public int getCount() {
        return splits.size();
    }

    @Override
    public Split getItem(int i) {
        return splits.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        // Check if the view has been created for the row. If not, lets inflate it
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_view_next_last_split_item, null); // List layout here
        }

        // Grab the TextViews in our custom layout
        TextView msg = (TextView) view.findViewById(R.id.msgTxt);
        TextView title = (TextView) view.findViewById(R.id.splitTitleTxt);
        TextView date = (TextView) view.findViewById(R.id.splitDateTxt);
        ImageView icon = (ImageView) view.findViewById(R.id.splitImageView);
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE dd/MM/yyyy");

        if (splits.get(i) != null && splits.get(i).getStatus().equals("lastdone"))
        {
            msg.setText("Last Workout");
            if (splits.get(i).getDateLast() != null)
                date.setText(sdf.format(splits.get(i).getDateLast()));
        }
        if (splits.get(i) != null && splits.get(i).getStatus().equals("scheduled")) {
            msg.setText("Next Scheduled Workout");
            Calendar cal = Calendar.getInstance();
            Date splitDay = cal.getTime();
            Calendar anotherCal = Calendar.getInstance();
            Date today = anotherCal.getTime();
            boolean found = false;
            int dayCount = 0;
            while (!found && dayCount < 7) {
                if (day == splitDay.getDay()) {
                    date.setText(sdf.format(cal.getTime()));
                    found = true;
                }
                dayCount++;
                cal.add(Calendar.DATE, 1);
                splitDay = cal.getTime();
            }
            //remove last added day to get actual day of split for comparison
            cal.add(Calendar.DATE, -1);
            splitDay = cal.getTime();
            if (splitDay.getDay() == today.getDay())
                msg.setText("Today's Scheduled Workout");

        }
        if (splits.get(i) != null) {
            title.setText(splits.get(i).getName());
            Resources res = view.getResources();
            String drawableName = splits.get(i).getIcon();
            int resID = res.getIdentifier(drawableName, "drawable", context.getPackageName());
            Drawable drawable = res.getDrawable(resID);
            icon.setImageDrawable(drawable);
        }
        return view;
    }

}