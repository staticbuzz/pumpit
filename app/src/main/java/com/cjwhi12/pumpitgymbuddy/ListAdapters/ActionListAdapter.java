package com.cjwhi12.pumpitgymbuddy.ListAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cjwhi12.pumpitgymbuddy.models.Action;
import com.cjwhi12.pumpitgymbuddy.R;

/**
 * Created by Christopher on 16/04/2015.
 */
public class ActionListAdapter extends BaseAdapter {
    private Context context;
    private Action[] actions;

    public ActionListAdapter(Context context, Action[] actions) {
        this.context = context;
        this.actions = actions;
    }

    @Override
    public int getCount() {
        return actions.length;
    }

    @Override
    public Action getItem(int i) {
        return actions[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        // Check if the view has been created for the row. If not, lets inflate it
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_view_actions_item, null); // List layout here
        }
        // Grab the TextViews in our custom layout
        TextView title = (TextView) view.findViewById(R.id.listActionTxt);
        ImageView image = (ImageView) view.findViewById(R.id.listImageView);
        // Assign values to the TextViews using the Reminder object
        title.setText(actions[i].getTitle());
        image.setImageDrawable(actions[i].getActionImageId());


        return view;
    }
}