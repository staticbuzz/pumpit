package com.cjwhi12.pumpitgymbuddy.ListAdapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cjwhi12.pumpitgymbuddy.databasehelpers.DatabaseHelper;
import com.cjwhi12.pumpitgymbuddy.models.ExerciseCategory;
import com.cjwhi12.pumpitgymbuddy.R;

/**
 * Created by Christopher on 26/05/2015.
 */
public class ExerciseCategoryListAdapter extends BaseAdapter {
    private Context context;
    private ExerciseCategory[] exercises;
    private DatabaseHelper dbh;
    private long split_id;

    public ExerciseCategoryListAdapter(Context context, ExerciseCategory[] exercises, long split_id) {
        this.context = context;
        this.exercises = exercises;
        this.split_id = split_id;
        dbh = new DatabaseHelper(context);
    }

    @Override
    public int getCount() {
        return exercises.length;
    }

    @Override
    public ExerciseCategory getItem(int i) {
        return exercises[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }



    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        // Check if the view has been created for the row. If not, lets inflate it
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_view_pic_and_title_item, null); // List layout here
        }

        // Grab the TextViews in our custom layout
        TextView title = (TextView) view.findViewById(R.id.exerciseName);
        ImageView icon = (ImageView) view.findViewById(R.id.addRemoveSplitImg);
        TextView countVal = (TextView) view.findViewById((R.id.countVal));
        // Assign values to the views using the exercisecategory object
        title.setText(exercises[i].getName());
        Resources res = view.getResources();
        String drawableName = exercises[i].getIcon();
        int resID = res.getIdentifier(drawableName, "drawable", context.getPackageName());
        Drawable drawable = res.getDrawable(resID);
        icon.setImageDrawable(drawable);
        String cat = getItem(i).getName().toLowerCase();
        int countEx = dbh.getExerciseInSplitCountForCategory(cat, split_id);
        countVal.setText(countEx+"");
        RelativeLayout parentRL = (RelativeLayout)countVal.getParent();
        if (countEx > 0)
        {
            parentRL.setBackgroundColor(context.getResources().getColor(R.color.sel_green));
        }
        else
            parentRL.setBackgroundColor(Color.WHITE);
        return view;
    }
}
