package com.cjwhi12.pumpitgymbuddy.ListAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.cjwhi12.pumpitgymbuddy.R;
import com.cjwhi12.pumpitgymbuddy.models.Exercise;

import java.util.ArrayList;

/**
 * Created by Christopher on 25/05/2015.
 */
public class EditSplitAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Exercise> allExercises;
    private ArrayList<Exercise> splitExercises;
    private long split_id;
    private boolean[] checkBoxState;

    public EditSplitAdapter(Context context, ArrayList<Exercise> allExercises,ArrayList<Exercise> splitExercises,long split_id) {
        this.context = context;
        this.allExercises = allExercises;
        this.splitExercises = splitExercises;
        this.split_id = split_id;
        checkBoxState = new boolean[allExercises.size()];
    }

    @Override
    public int getCount() {
        return allExercises.size();
    }

    @Override
    public Object getItem(int i) {
        return checkBoxState[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ExerciseSplitViewHolder viewHolder = null;

        //determine if exercise should be checked initially
        boolean checkMe = findValueInArrayList(splitExercises, allExercises.get(i).getId());
        //save this checked state to array for re-use
        checkBoxState[i] = checkMe;

        // Check if the view has been created for the row. If not, lets inflate it
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_view_ex_with_checkbox_item, null); // List layout here

            // Grab the TextViews in our custom layout
            TextView title = (TextView) view.findViewById(R.id.exerciseName);
            Switch addSplitCB = (Switch) view.findViewById(R.id.addSplitCB);

            viewHolder = new ExerciseSplitViewHolder();
            viewHolder.setSwitch(addSplitCB);
            viewHolder.setTextView(title);

            viewHolder.getSwitch().setChecked(checkMe);
            viewHolder.getSwitch().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    int getPosition = (Integer) buttonView.getTag();  // Here we get the position that we have set for the checkbox using setTag.
                    checkBoxState[getPosition] = buttonView.isChecked();
                }
            });
            view.setTag(viewHolder);
            view.setTag(R.id.exerciseName,viewHolder.getTextView());
            view.setTag(R.id.addSplitCB,viewHolder.getSwitch());

        } else
            viewHolder = (ExerciseSplitViewHolder) view.getTag();

        viewHolder.getSwitch().setTag(i);
        viewHolder.getTextView().setText(allExercises.get(i).getName());

        /* this line so important - after listview items get recycled, all the checkbox states can get jumbled up. this line resets them back to
        ** correct values.
        */
        viewHolder.getSwitch().setChecked(checkBoxState[i]);


        return view;
    }


    public static boolean findValueInArrayList(ArrayList<Exercise> arrayList, long value) {
        boolean found = false;
        for(Exercise e: arrayList){
            if(e.getId() == value)
                found = true;
        }
        return found;
    }

    /** Holds child views for one row. */
    private static class ExerciseSplitViewHolder {
        private Switch inSplitSwitch;
        private TextView exerciseNameTV ;

        public ExerciseSplitViewHolder() {
            //
        }

        public Switch getSwitch() {
            return inSplitSwitch;
        }

        public void setSwitch(Switch inSplitSwitch) {
            this.inSplitSwitch = inSplitSwitch;
        }

        public TextView getTextView() {
            return exerciseNameTV;
        }

        public void setTextView(TextView exerciseNameTV) {
            this.exerciseNameTV = exerciseNameTV;
        }
    }



}
