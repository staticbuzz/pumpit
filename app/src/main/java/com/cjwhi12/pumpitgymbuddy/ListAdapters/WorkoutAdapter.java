package com.cjwhi12.pumpitgymbuddy.ListAdapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cjwhi12.pumpitgymbuddy.R;
import com.cjwhi12.pumpitgymbuddy.models.Exercise;
import com.cjwhi12.pumpitgymbuddy.models.WorkoutResult;

import java.util.ArrayList;

/**
 * Created by Christopher on 2/06/2015.
 */
public class WorkoutAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Exercise> exercises;
    private ArrayList<WorkoutResult> results;
    private ArrayList<WorkoutResult> prevResults;


    public WorkoutAdapter(Context context, ArrayList<Exercise> exercises, ArrayList<WorkoutResult> results, ArrayList<WorkoutResult> prevResults){
        this.context = context;
        this.exercises = exercises;
        this.results = results;
        this.prevResults = prevResults;

    }

    @Override
    public int getCount() {
        return exercises.size();
    }

    @Override
    public Exercise getItem(int i) {
        return exercises.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        WorkOutItemViewHolder viewHolder = null;

        // Check if the view has been created for the row. If not, lets inflate it
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_view_workout_item, null); // List layout here



            // Grab the TextViews in our custom layout
            TextView title = (TextView) view.findViewById(R.id.exerciseName);
            TextView unit = (TextView) view.findViewById(R.id.unitLbl);
            TextView unitNew = (TextView) view.findViewById(R.id.unitLbl2);
            TextView reps = (TextView) view.findViewById(R.id.repLbl);
            TextView repsNew = (TextView) view.findViewById(R.id.repLbl2);

            TextView prevUnitTxt = (TextView) view.findViewById(R.id.prevUnitResTxt);
            TextView prevRepsTxt = (TextView) view.findViewById(R.id.prevRepsTxt);
            TextView nextUnitTxt = (TextView) view.findViewById(R.id.curUnitResTxt);
            TextView nextRepsTxt = (TextView) view.findViewById(R.id.curRepsTxt);

            viewHolder = new WorkOutItemViewHolder();

            viewHolder.setTitleLbl(title);
            viewHolder.setUnitLbl(unit);
            viewHolder.setUnitNewLbl(unitNew);
            viewHolder.setRepsLbl(reps);
            viewHolder.setRepsNewLbl(repsNew);
            viewHolder.setPrevUnitTxt(prevUnitTxt);
            viewHolder.setNextUnitTxt(nextUnitTxt);
            viewHolder.setPrevRepsTxt(prevRepsTxt);
            viewHolder.setNextRepsTxt(nextRepsTxt);

            view.setTag(viewHolder);

        }
        else
        {
            viewHolder = (WorkOutItemViewHolder) view.getTag();
        }

        //these two lines allow us to get the position within the listview of the parent listitem of these edittexts
        viewHolder.getNextRepsTxt().setTag(i);
        viewHolder.getNextUnitTxt().setTag(i);

        // Assign values to the views using the exercise object
        viewHolder.getTitleLbl().setText(exercises.get(i).getName());
        viewHolder.getUnitLbl().setText(exercises.get(i).getUnit());
        viewHolder.getUnitNewLbl().setText(exercises.get(i).getUnit());

        if (exercises.get(i).getUnit().equals("km")) {
            viewHolder.getRepsLbl().setText("mins");
            viewHolder.getRepsNewLbl().setText("mins");
        }

        /* these lines very important - after listview items get recycled, all the edittext states can get jumbled up. this line resets them back to
        ** correct values.
        */
        if (prevResults.get(i) == null || prevResults.get(i).getReps() == 0)
            viewHolder.getPrevRepsTxt().setText("N/A");
        else
            viewHolder.getPrevRepsTxt().setText(prevResults.get(i).getReps() + "");

        if (results.get(i).getReps() == 0.0)
            viewHolder.getNextRepsTxt().setText("");
        else
            viewHolder.getNextRepsTxt().setText(results.get(i).getReps() + "");

        if (prevResults.get(i) == null || prevResults.get(i).getResult() == 0.0)
            viewHolder.getPrevUnitTxt().setText("N/A");
        else
            viewHolder.getPrevUnitTxt().setText(prevResults.get(i).getResult() + "");

        if (results.get(i).getResult() == 0.0)
            viewHolder.getNextUnitTxt().setText("");
        else
            viewHolder.getNextUnitTxt().setText(results.get(i).getResult()+"");

        return view;
    }

    //Holds child views for one row. The viewholder pattern speeds up listview scrolling quite a bit
    private static class WorkOutItemViewHolder {
        // Grab the Views in our custom layout
        private TextView titleLbl;
        private TextView unitLbl;
        private TextView unitNewLbl;
        private TextView repsLbl;
        private TextView repsNewLbl;

        private TextView prevUnitTxt;
        private TextView prevRepsTxt;
        private TextView nextUnitTxt;
        private TextView nextRepsTxt;

        public WorkOutItemViewHolder() {
            //required empty constructor
        }


        //getters
        public TextView getTitleLbl() {
            return titleLbl;
        }

        public TextView getUnitLbl() {
            return unitLbl;
        }
        public TextView getUnitNewLbl() {
            return unitNewLbl;
        }
        public TextView getRepsLbl() {
            return repsLbl;
        }
        public TextView getRepsNewLbl() {
            return repsNewLbl;
        }

        public TextView getPrevUnitTxt() {
            return prevUnitTxt;
        }
        public TextView getPrevRepsTxt() {
            return prevRepsTxt;
        }
        public TextView getNextUnitTxt() {
            return nextUnitTxt;
        }
        public TextView getNextRepsTxt() {
            return nextRepsTxt;
        }



        //setters
        public void setTitleLbl(TextView titleLbl) {
            this.titleLbl = titleLbl;
        }

        public void setUnitLbl(TextView unitLbl) {
            this.unitLbl = unitLbl;
        }
        public void setUnitNewLbl(TextView unitNewLbl) {
            this.unitNewLbl = unitNewLbl;
        }
        public void setRepsLbl(TextView repsLbl) {
            this.repsLbl = repsLbl;
        }
        public void setRepsNewLbl(TextView repsNewLbl) {
            this.repsNewLbl = repsNewLbl;
        }

        public void setPrevUnitTxt(TextView prevUnitTxt) {
            this.prevUnitTxt = prevUnitTxt;
        }
        public void setPrevRepsTxt(TextView prevRepsTxt) {
            this.prevRepsTxt = prevRepsTxt;
        }
        public void setNextUnitTxt(TextView nextUnitTxt) {
            this.nextUnitTxt = nextUnitTxt;
        }
        public void setNextRepsTxt(TextView nextRepsTxt) {
            this.nextRepsTxt = nextRepsTxt;
        }

    }


}
