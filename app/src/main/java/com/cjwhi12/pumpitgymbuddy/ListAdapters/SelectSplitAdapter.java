package com.cjwhi12.pumpitgymbuddy.ListAdapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cjwhi12.pumpitgymbuddy.R;
import com.cjwhi12.pumpitgymbuddy.databasehelpers.DatabaseHelper;
import com.cjwhi12.pumpitgymbuddy.models.Split;

import java.util.ArrayList;

/**
 * Created by Christopher on 25/05/2015.
 */
public class SelectSplitAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Split> splits;
    private DatabaseHelper dbh;
    public SelectSplitAdapter(Context context, ArrayList<Split> splits) {
        this.context = context;
        this.splits = splits;
        dbh = new DatabaseHelper(context);

    }

    @Override
    public int getCount() {
        return splits.size();
    }

    @Override
    public Split getItem(int i) {
        return splits.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }




    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        // Check if the view has been created for the row. If not, lets inflate it
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_view_pic_and_title_item, null); // List layout here
        }

        // Grab the TextViews in our custom layout
        TextView title = (TextView) view.findViewById(R.id.exerciseName);
        ImageView icon = (ImageView) view.findViewById(R.id.addRemoveSplitImg);
        TextView countVal = (TextView) view.findViewById((R.id.countVal));

        // Assign values to the views using the Split object
        title.setText(splits.get(i).getName());
        Resources res = view.getResources();
        String drawableName = splits.get(i).getIcon();
        int resID = res.getIdentifier(drawableName, "drawable", context.getPackageName());
        Drawable drawable = res.getDrawable(resID);
        icon.setImageDrawable(drawable);
        int countEx = dbh.getExerciseCountForSplit(getItem(i).getId());
        countVal.setText(countEx + "");
        if (countEx == 0)
        {
            countVal.setTextColor(Color.RED);
        }
        else
            countVal.setTextColor(context.getResources().getColor(R.color.dk_green));

        return view;
    }

}
