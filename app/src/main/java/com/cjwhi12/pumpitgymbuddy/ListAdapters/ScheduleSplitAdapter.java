package com.cjwhi12.pumpitgymbuddy.ListAdapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cjwhi12.pumpitgymbuddy.models.Action;
import com.cjwhi12.pumpitgymbuddy.R;
import com.cjwhi12.pumpitgymbuddy.models.Split;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Christopher on 16/04/2015.
 */
public class ScheduleSplitAdapter extends BaseAdapter {
    private Context context;
    private String[] days;
    private Split[] scheduledSplits;

    public ScheduleSplitAdapter(Context context, Split[] scheduledSplits) {
        this.context = context;
        this.scheduledSplits = scheduledSplits;
        days = new String[]{"Mon","Tue","Wed","Thu","Fri","Sat","Sun"};

    }

    @Override
    public int getCount() {
        return days.length;
    }

    @Override
    public String getItem(int i) {
        return days[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        SchedSplitViewHolder viewHolder = null;

        // Check if the view has been created for the row. If not, lets inflate it
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_view_split_schedule_item, null); // List layout here

            // Grab the TextViews in our custom layout
            TextView title = (TextView) view.findViewById(R.id.dayName);
            TextView split = (TextView) view.findViewById(R.id.splitName);
            ImageView image = (ImageView) view.findViewById(R.id.addRemoveSplitImg);

            viewHolder = new SchedSplitViewHolder();
            viewHolder.setDayWeekTV(title);
            viewHolder.setSplitNameTV(split);
            viewHolder.setImageView(image);



            view.setTag(viewHolder);


        } else
            viewHolder = (SchedSplitViewHolder) view.getTag();

        if (scheduledSplits[i] != null){
            viewHolder.getSplitNameTV().setText(scheduledSplits[i].getName());
            viewHolder.getImageView().setImageDrawable(context.getResources().getDrawable(R.drawable.close128));
        }
        else
        {
            viewHolder.getSplitNameTV().setText("...");
            viewHolder.getImageView().setImageDrawable(context.getResources().getDrawable(R.drawable.add128));
        }
        viewHolder.getDayWeekTV().setText(days[i]);

        return view;
    }

    /** Holds child views for one row. */
    private static class SchedSplitViewHolder {
        private TextView splitNameTV;
        private TextView dayWeekTV;
        private ImageView imageView;

        public SchedSplitViewHolder() {
            //
        }

        public TextView getSplitNameTV() {
            return splitNameTV;
        }

        public void setSplitNameTV(TextView splitNameTV) {
            this.splitNameTV = splitNameTV;
        }

        public TextView getDayWeekTV() {
            return dayWeekTV;
        }

        public void setDayWeekTV(TextView dayWeekTV) {
            this.dayWeekTV = dayWeekTV;
        }

        public ImageView getImageView() {return imageView;}

        public void setImageView(ImageView imageView){
            this.imageView = imageView;
        }

    }
}